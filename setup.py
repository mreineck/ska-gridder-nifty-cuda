import os
import platform
import tempfile
from setuptools import setup, Extension
from setuptools.command.build_ext import build_ext
from numpy import get_include

include_dirs = ["./", "./src/", get_include()]
extra_compile_args = ["-Wall", "-Wextra", "-O3", "-std=c++11"]
python_module_link_args = []

if platform.system() == "Darwin":
    extra_compile_args += ["-mmacosx-version-min=10.9"]
    python_module_link_args += ["-ldl"]
else:
    python_module_link_args += ["-Wl,-rpath,$ORIGIN"]
    python_module_link_args += ["-ldl"]


class BuildExt(build_ext):
    def build_extension(self, ext):
        c_sources = [x for x in ext.sources
                     if x.endswith(".c") or x.endswith(".cpp")]
        cu_sources = [x for x in ext.sources if x.endswith(".cuh")]
        wrapped_cu_sources = []
        for cu in cu_sources:
            text = ""
            names = []

            # Read contents of CUDA source file.
            with open(cu, "r", encoding="utf-8") as fh:
                while True:
                    line = fh.readline()
                    if not line:
                        break
                    if line.startswith("REGISTER_NAME"):
                        names.append(line)
                    else:
                        text += line

            # Create the reformatted text.
            text = \
                '#include "registrar.h"\n' +\
                'static const char src[] = R"(\n' +\
                text +\
                ')";\n' +\
                'REGISTER_SRC(src)\n'
            for name in names:
                text += name

            # Write text to a named temp file.
            (temp, temp_path) = tempfile.mkstemp(suffix=".cpp")
            os.write(temp, text.encode("utf-8"))
            os.close(temp)
            wrapped_cu_sources.append(temp_path)

        # Append the wrapped CUDA sources.
        ext.sources = c_sources + wrapped_cu_sources

        # Call the base class.
        build_ext.build_extension(self, ext)

        # Delete the tempfiles.
        for file in wrapped_cu_sources:
            os.remove(file)


def get_extension_modules():
    return [
        Extension(
            "_cuda_nifty_gridder_lib",
            sources=[
                "src/dirty2ms_2d.cpp",
                "src/dirty2ms_3d.cpp",
                "src/nifty_kernels.cuh",
                "src/ms2dirty_2d.cpp",
                "src/ms2dirty_3d.cpp",
                "src/nifty_utils.cpp",
                "src/wrap_lib.cpp",
                "src/wrap_gpu.cpp",
                "src/wrap_python.cpp",
                "src/wrap_threads.cpp",
            ],
            depends=["setup.py"],
            include_dirs=include_dirs,
            extra_compile_args=extra_compile_args,
            extra_link_args=python_module_link_args
        )
    ]

setup(
    name="ska-gridder-nifty-cuda",
    version="0.3.1",
    description="CUDA Nifty GPU Gridder",
    packages=["cuda_nifty_gridder"],
    setup_requires=["numpy"],
    ext_modules=get_extension_modules(),
    install_requires=["numpy"],
    cmdclass={'build_ext': BuildExt}
)
