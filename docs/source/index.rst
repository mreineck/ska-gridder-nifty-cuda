.. CUDA Nifty Gridder documentation master file, created by
   sphinx-quickstart on Tue Nov  9 16:40:28 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden: 


.. README =============================================================

.. This project most likely has its own README. We include it here.

.. toctree::
   :maxdepth: 2
   :caption: Readme

   ../../README

.. COMMUNITY SECTION ==================================================

..

.. toctree::
  :maxdepth: 2
  :caption: CUDA Nifty Gridder
  :hidden:

  package/guide


CUDA Nifty Gridder
==================================

This the user documentation for the CUDA Nifty Gridder.

- :doc:`package/guide`




