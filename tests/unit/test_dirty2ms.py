import concurrent.futures
import numpy as np
import cuda_nifty_gridder as cng 
import ducc0.wgridder as rng

def RRMSE(est, actual):
	return np.linalg.norm(est - actual)/np.linalg.norm(actual)

def compareCNGandRNG(uvw, freqs, test_dirty_image, weights, imSize, pixsize_rad, epsilon, doWStacking = True):

	#print("")
	#print("Running RNG dirty2ms()...")
	#print("")
	rngVis = rng.dirty2ms(uvw, freqs, test_dirty_image, weights, pixsize_rad, pixsize_rad, 0, 0, epsilon, doWStacking, verbosity = 0)

	#print("")
	#print("Running CNG dirty2ms()...")
	#print("")
	cngVis = cng.dirty2ms(uvw, freqs, test_dirty_image, weights, pixsize_rad, pixsize_rad, 0, 0, epsilon, doWStacking, verbosity = 0)

	return RRMSE(cngVis, rngVis)

def run_dirty2ms(doSingle=True, multiChannel=True, epsilon=1e-5, doWStacking = True):
	CEND   = '\33[0m'
	CRED   = '\33[31m'
	CGREEN = '\33[92m'

	# Read test visibilities.
	if multiChannel:
		test_data = np.load("vla_d_3_chan.npz")
		strChannel = "multi  channel"
	else:
		test_data = np.load("vla_d.npz")
		strChannel = "single channel"
	vis = test_data["vis"]
	freqs = test_data["freqs"]
	uvw = test_data["uvw"]

	#print("Shape of visibility array (num_rows, num_chan): {}".format(vis.shape))
	#print("Channel frequencies (Hz): {}".format(freqs))

	test_dirty_image = np.load("testDirtyImage_D.npy")
	#print("Shape of dirty image (num_rows, num_cols): {}".format(test_dirty_image.shape))

	if doSingle:
		# Convert data to single precision.
		#vis = vis.astype(np.complex64)
		test_dirty_image = test_dirty_image.astype(np.float32)
		weights = np.ones_like(vis, dtype=np.float32)
		strPrecision = "single"
	else:
		weights = np.ones_like(vis, dtype=np.float64)
		strPrecision = "double"

	if doWStacking:
		strGridderType = "3D"
	else:
		strGridderType = "2D"
	
	passThreshold = 2*epsilon

	imSize = 1024
	pixsize_deg = 1.94322419749866394E-02
	pixsize_rad = pixsize_deg * np.pi / 180.0

	strTestSource = "reference code"

	#print("")
	#print("Running test against %s with %s precision..." % (strTestSource, strPrecision))
	#print("")
	#print("    dirty2ms() as %s precision, %s gridder, with %s data and ε = %.0e, RRMSE is: %e. " % (strPrecision, strGridderType, strChannel, epsilon, 0), end = "");

	error = compareCNGandRNG(uvw, freqs, test_dirty_image, weights, imSize, pixsize_rad, epsilon, doWStacking)

	print("    dirty2ms() as %s precision, %s gridder, with %s data and ε = %.0e, RRMSE is: %e. " % (strPrecision, strGridderType, strChannel, epsilon, error), end = "");

	if error < passThreshold:
		print(CGREEN + "PASS" + CEND)
	else:
		print(CRED   + "FAIL" + CEND)


	return error, passThreshold

def test_dirty2ms_single_2D():

	es = range(1, 6)

	print() # this line formats better when -s is used with pytest

	for e in range(0, len(es)):

		thisEps = 10**(-es[e])

		error, passThreshold = run_dirty2ms(doSingle=True, multiChannel=True, epsilon=thisEps, doWStacking = False)
		assert(error < passThreshold)

def test_dirty2ms_single_3D():

	#es = [3, 6, 9, 13]
	es = range(1, 6)

	print() # this line formats better when -s is used with pytest

	for e in range(0, len(es)):

		thisEps = 10**(-es[e])

		error, passThreshold = run_dirty2ms(doSingle=True, multiChannel=True, epsilon=thisEps, doWStacking = True)
		assert(error < passThreshold)

def test_dirty2ms_double_2D():

	#es = [3, 6, 9, 13]
	es = range(5, 13)

	print() # this line formats better when -s is used with pytest

	for e in range(0, len(es)):

		thisEps = 10**(-es[e])

		error, passThreshold = run_dirty2ms(doSingle=False, multiChannel=True, epsilon=thisEps, doWStacking = False)
		assert(error < passThreshold)

def test_dirty2ms_double_3D():

	#es = [3, 6, 9, 13]
	es = range(5, 13)

	print() # this line formats better when -s is used with pytest

	for e in range(0, len(es)):

		thisEps = 10**(-es[e])

		error, passThreshold = run_dirty2ms(doSingle=False, multiChannel=True, epsilon=thisEps, doWStacking = True)
		assert(error < passThreshold)

def test_dirty2ms_double_3D_threadsafe():

	#es = [3, 6, 9, 13]
	es = range(5, 13)

	print() # this line formats better when -s is used with pytest

	with concurrent.futures.ThreadPoolExecutor() as executor:
		futures = []
		for e in range(0, len(es)):
			thisEps = 10**(-es[e])
			f = executor.submit(run_dirty2ms, doSingle=False, multiChannel=True, epsilon=thisEps, doWStacking = True)
			futures.append(f)
		for f in futures:
			error, passThreshold = f.result()
			assert(error < passThreshold)
