#!/bin/bash

COMMAND=${1}
BUILD_TYPE=${2}

PROJECT_DIR=${PWD}

case ${COMMAND} in

    build_and_run)
        cd ${PROJECT_DIR} 
        rm -rf build
        mkdir build
        cd build && cmake -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ..
        make -j8
        imaging/./nifty_gridder
        cd ${PROJECT_DIR}
    ;;

    build)
        cd ${PROJECT_DIR} 
        rm -rf build
        mkdir build
        cd build && cmake -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ..
        make -j8
        cd ${PROJECT_DIR}
    ;;

    run)
        cd ${PROJECT_DIR} 
        cd build/imaging
        ./nifty_gridder
        cd ${PROJECT_DIR} 
    ;;

    clean)
        cd ${PROJECT_DIR} 
        rm -rf build
        cd ${PROJECT_DIR} 
    ;;
    
    *)
        echo "ERR: Unrecognized command, please review this script for valid commands..."
    ;;

esac