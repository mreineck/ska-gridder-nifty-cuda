from matplotlib import pyplot as plt
import numpy as np
import csv

rows = 1
cols = 2

f = plt.figure()

dirty_image_binary = np.fromfile("CUDA_dirty_image_0_1024_D.bin", dtype=np.float64)
original = dirty_image_binary.reshape(1024, 1024) # customize as required
# original = np.fft.fftshift(original)
# original = np.fft.fft2(original)
# original = np.fft.fftshift(original)
f.add_subplot(rows, cols, 1)
plt.title('Original')
plt.imshow(original, cmap='hot', interpolation='none')
plt.colorbar()

dirty_image_binary = np.fromfile("CUDA_dirty_image_1_1024_D.bin", dtype=np.float64)
predicted = dirty_image_binary.reshape(1024, 1024) # customize as required
# predicted = np.fft.fftshift(predicted)
# predicted = np.fft.fft2(predicted)
# predicted = np.fft.fftshift(predicted)
f.add_subplot(rows, cols, 2)
plt.title('Predicted')
plt.imshow(predicted, cmap='hot', interpolation='none')
plt.colorbar()

# f.add_subplot(rows, cols, 3)
# plt.title('P/O')
# plt.imshow(predicted/original, cmap='hot', interpolation='none')
# plt.colorbar()

# print(np.min(predicted/original))
# print(np.max(predicted/original))
# min = np.argmin(predicted/original)
# print(min)

plt.show()