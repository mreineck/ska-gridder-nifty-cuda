/* See the LICENSE file at the top-level directory of this distribution. */

#ifndef MS2DIRTY_2D_H_
#define MS2DIRTY_2D_H_

#include "wrap_gpu.h"

#ifdef __cplusplus
extern "C" {
#endif

void ms2dirty_2d(WrapCUDA& wrapper, int num_rows, int num_chan, const Mem* uvw, const Mem* freq_hz,
        const Mem* ms, const Mem* weight, int npix_x, int npix_y,
        double pixsize_x_rad, double pixsize_y_rad, double epsilon,
        Mem* im, int verbosityLevel, int* status);

#ifdef __cplusplus
}
#endif

#endif /* include guard */
