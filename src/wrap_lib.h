/* See the LICENSE file at the top-level directory of this distribution. */

#ifdef __cplusplus
extern "C" {
#endif

void* lib_open(const char* name);

int lib_close(void* handle);

void* lib_symbol(void* handle, const char* symbol);

#ifdef __cplusplus
}
#endif
