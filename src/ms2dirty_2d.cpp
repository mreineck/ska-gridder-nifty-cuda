/* See the LICENSE file at the top-level directory of this distribution. */

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdint>

#include "ms2dirty_2d.h"
#include "nifty_utils.h"

void ms2dirty_2d(WrapCUDA& wrapper, int num_rows, int num_chan, const Mem* uvw, const Mem* freq_hz,
        const Mem* ms, const Mem* weight, int npix_x, int npix_y,
        double pixsize_x_rad, double pixsize_y_rad, double epsilon,
        Mem* im, int verbosityLevel, int* status)
{
    int start_row = 0;
    size_t num_threads[] = {1, 1, 1}, num_blocks[] = {1, 1, 1};
    //(void)epsilon;
    if (*status) return;

    // Check for supported options.
    if (npix_x != npix_y || pixsize_x_rad != pixsize_y_rad)
    {
        fprintf(stderr, "Only square images are currently supported\n");
        *status = -1000;
        return;
    }

    const double upsampling = 2.0;
    const int image_size = npix_x;

    const int vis_type = mem_type(ms);
    const int vis_precision = (vis_type & MEM_DOUBLE) ? MEM_DOUBLE : MEM_FLOAT;

	double beta;
	int support;
	int grid_size;
	
	CalculateParamsFromEpsilon(epsilon, image_size, vis_precision, 
					   grid_size, support, beta, *status);	

	//CalculateSupportAndBeta(upsampling, epsilon, support, beta, *status);

	LogF(verbosityLevel, 1, "support is %i\n", support);
	LogF(verbosityLevel, 1, "beta is %e\n", beta);
    beta *= support; 
	
    //const int max_rows_per_chunk = 2000000 / num_chan;
    const int max_rows_per_chunk = num_rows;
    const int chunk_size = num_rows;
    const int num_w_grids_batched = 1;
    const int coord_type = mem_type(uvw);
    //const int vis_type = mem_type(ms);
    //const int vis_precision = (vis_type & MEM_DOUBLE) ? MEM_DOUBLE : MEM_FLOAT;
    const int dbl_vis = (vis_type & MEM_DOUBLE);
    const int dbl_coord = (coord_type & MEM_DOUBLE);
    //const int image_size = npix_x;
   //grid_size = floor(npix_x * upsampling);
    const double uv_scale = grid_size * pixsize_x_rad;
    const double pixel_size = pixsize_x_rad;
    const float beta_f = (float) beta;
    const float uv_scale_f = (float) uv_scale;
    const float pixel_size_f = (float) pixel_size;

    // not doing w-stacking
	const double min_plane_w = 0.0;
	const int num_total_w_grids = 1;
	double w_scale = 1.0; // scaling factor for converting w coord to signed w grid index
    const double inv_w_scale = 1.0 / w_scale;
    const float inv_w_scale_f = (float) inv_w_scale;
    const float w_scale_f = (float) w_scale;
    const float min_plane_w_f = (float) min_plane_w;

	if (1)
	{	
		LogF(verbosityLevel, 1, "grid_size is %i\n",  grid_size);
		LogF(verbosityLevel, 1, "image_size is %i\n", image_size);
		LogF(verbosityLevel, 1, "num_total_w_grids is %i\n", num_total_w_grids);
		LogF(verbosityLevel, 1, "min_plane_w is %e\n", min_plane_w);
	}

    // Create the empty grid.
    size_t num_w_grid_stack_cells = grid_size * grid_size * num_w_grids_batched;
    Mem* d_w_grid_stack = wrapper.mem_create(vis_type, MEM_GPU,
            num_w_grid_stack_cells, status);

    // Create the empty image.
    Mem* d_dirty_image = wrapper.mem_create(vis_precision, MEM_GPU,
            npix_x * npix_y, status);
    wrapper.mem_clear_contents(d_dirty_image, status);

    // Device scratch memory.
    Mem* d_freq_hz = wrapper.mem_create_copy(freq_hz, MEM_GPU, status);
    Mem* d_uvw = wrapper.mem_create(coord_type,
            MEM_GPU, max_rows_per_chunk * 3, status);
    Mem* d_ms = wrapper.mem_create(vis_type,
            MEM_GPU, max_rows_per_chunk * num_chan, status);
    Mem* d_weight = wrapper.mem_create(vis_precision,
            MEM_GPU, max_rows_per_chunk * num_chan, status);
    wrapper.mem_copy_contents(d_uvw, uvw, 0, start_row * 3,
            chunk_size * 3, status);
    wrapper.mem_copy_contents(d_ms, ms, 0, start_row * num_chan,
            chunk_size * num_chan, status);
    wrapper.mem_copy_contents(d_weight, weight, 0, start_row * num_chan,
            chunk_size * num_chan, status);

    // Create the FFT plan.
    const int fft_dims[] = {grid_size, grid_size};
    FFT* fft = wrapper.fft_create(vis_precision, MEM_GPU, 2, fft_dims, FFT_C2C,
            num_w_grids_batched, status);

    // Determine how many w grid subset batches to process in total
    const int total_w_grid_batches =
            (num_total_w_grids + num_w_grids_batched - 1) / num_w_grids_batched;
			
	//const int total_w_grid_batches = 1;  // AG for testing (comment out otherwise)

    for (int batch = 0; batch < total_w_grid_batches; batch++)
    {
        const int num_w_grids_subset = std::min(
            num_w_grids_batched,
            num_total_w_grids - ((batch * num_w_grids_batched) % num_total_w_grids)
        );
        const int grid_start_w = batch * num_w_grids_batched;
        wrapper.mem_clear_contents(d_w_grid_stack, status);

        // Perform gridding on a "chunk" of w grids
        {
            const char* k = 0;
            if (dbl_vis && dbl_coord)
                k = "nifty_gridding_2d<double, double2, double, double2, double3>";
            else if (!dbl_vis && dbl_coord)
                k = "nifty_gridding_2d<float, float2, double, double2, double3>";
            else if (!dbl_vis && !dbl_coord)
                k = "nifty_gridding_2d<float, float2, float, float2, float3>";
            if (k)
            {
                num_threads[0] = 1;
                num_threads[1] = 256;
                num_blocks[0] = (num_chan + num_threads[0] - 1) / num_threads[0];
                num_blocks[1] = (chunk_size + num_threads[1] - 1) / num_threads[1];
                const bool solving = 1;
                const void* args[] = {
                    &chunk_size,
                    &num_chan,
                    mem_buffer_const(d_ms),
                    mem_buffer_const(d_weight),
                    mem_buffer_const(d_uvw),
                    mem_buffer_const(d_freq_hz),
                    mem_buffer(d_w_grid_stack),
                    &grid_size,
                    &grid_start_w,
                    &num_w_grids_subset,
                    &support,
                    dbl_vis ? (const void*)&beta : (const void*)&beta_f,
                    dbl_coord ?
                        (const void*)&uv_scale : (const void*)&uv_scale_f,
                    dbl_coord ?
                        (const void*)&w_scale : (const void*)&w_scale_f,
                    dbl_coord ?
                        (const void*)&min_plane_w : (const void*)&min_plane_w_f,
                    &solving
                };
                wrapper.launch_kernel(k, num_blocks, num_threads, 0, 0, args, status);
            }
        }
		
		if (0)
		{
			printf("Trying here...");
			wrapper.mem_copy_contents(im, d_w_grid_stack, 0, 0, (npix_x*2 * npix_y*2), status);  // AG for testing (comment out otherwise)
			printf("success!!\n");
		}

        // Perform 2D FFT on each bound w grid
        wrapper.fft_exec(fft, d_w_grid_stack, d_w_grid_stack, 0, status);

        // Perform phase shift on a "chunk" of planes and sum into single real plane
        {
            const char* k = dbl_vis ?
                    "apply_w_screen_and_sum<double, double2>" :
                    "apply_w_screen_and_sum<float, float2>";
            num_threads[0] = std::min(32, (npix_x + 1) / 2);
            num_threads[1] = std::min(32, (npix_y + 1) / 2);
            // Allow extra in negative x quadrants, for asymmetric image centre
            num_blocks[0] = (npix_x / 2 + 1 + num_threads[0] - 1) / num_threads[0];
            num_blocks[1] = (npix_y / 2 + 1 + num_threads[1] - 1) / num_threads[1];
			const bool do_FFT_shift = true;
			const bool do_wstacking = false;
            const void* args[] = {
                mem_buffer(d_dirty_image),
                &image_size,
                dbl_vis ?
                    (const void*)&pixel_size : (const void*)&pixel_size_f,
                mem_buffer_const(d_w_grid_stack),
                &grid_size,
                &grid_start_w,
                &num_w_grids_subset,
                dbl_vis ?
                    (const void*)&inv_w_scale : (const void*)&inv_w_scale_f,
                dbl_vis ?
                    (const void*)&min_plane_w : (const void*)&min_plane_w_f,
				&do_FFT_shift,
				&do_wstacking
            };
            wrapper.launch_kernel(k, num_blocks, num_threads, 0, 0, args, status);
        }
    }

    //mem_copy_contents(im, d_dirty_image, 0, 0, npix_x * npix_y, status);  // AG 

    // Free FFT plan and data.
    wrapper.fft_free(fft, status);

    // Free device input arrays.
    wrapper.mem_free(d_freq_hz, status);
    wrapper.mem_free(d_uvw, status);
    wrapper.mem_free(d_ms, status);
    wrapper.mem_free(d_weight, status);
    wrapper.mem_free(d_w_grid_stack, status);

#if 0
    // Get sum of visibility weights.
    double sum = 0.0;
    const int num_points = num_rows * num_chan;
    if (mem_type(weight) == MEM_FLOAT)
    {
        const float* wt = (const float*) mem_ptr_const(weight);
        for (int i = 0; i < num_points; ++i)
            sum += wt[i];
    }
    else
    {
        const double* wt = (const double*) mem_ptr_const(weight);
        for (int i = 0; i < num_points; ++i)
            sum += wt[i];
    }

    // Normalise image.
    {
        const char* k = dbl_vis ? "scale<double>" : "scale<float>";
        const unsigned int num = image_size * image_size;
        num_threads[0] = 256;
        num_threads[1] = 1;
        num_blocks[0] = (num + num_threads[0] - 1) / num_threads[0];
        num_blocks[1] = 1;
        const double scale_val = 1.0 / sum;
        const float scale_val_f = (float) scale_val;
        const void* args[] = {
            &num,
            dbl_vis ? (const void*)&scale_val : (const void*)&scale_val_f,
            mem_buffer(d_dirty_image)
        };
        wrapper.launch_kernel(k, num_blocks, num_threads, 0, 0, args, status);
    }
#endif

    // Generate Gauss Legendre kernel for convolution correction.
    double *quadrature_kernel, *quadrature_nodes, *quadrature_weights;
    double *conv_corr_kernel;
    quadrature_kernel  = (double*) calloc(QUADRATURE_SUPPORT_BOUND, sizeof(double));
    quadrature_nodes   = (double*) calloc(QUADRATURE_SUPPORT_BOUND, sizeof(double));
    quadrature_weights = (double*) calloc(QUADRATURE_SUPPORT_BOUND, sizeof(double));
    conv_corr_kernel   = (double*) calloc(image_size / 2 + 1, sizeof(double));
    generate_gauss_legendre_conv_kernel(image_size, grid_size, support, beta,
            quadrature_kernel, quadrature_nodes, quadrature_weights,
            conv_corr_kernel);

    // Need to determine normalisation factor for scaling runtime calculated
    // conv correction values for coordinate n (where n = sqrt(1 - l^2 - m^2) - 1)
    //uint32_t p = (uint32_t)(ceil(1.5 * support + 2.0));
    uint32_t p = (uint32_t)(int(1.5 * support + 2.0));
    double conv_corr_norm_factor = 0.0;
    for (uint32_t i = 0; i < p; i++)
        conv_corr_norm_factor += quadrature_kernel[i] * quadrature_weights[i];
    conv_corr_norm_factor *= (double)support;
    const float conv_corr_norm_factor_f = (float)conv_corr_norm_factor;

    // Need to determine final scaling factor for scaling dirty image by w grid accumulation
    const double inv_w_range = 1.0; //   / (max_plane_w - min_plane_w);
    const float inv_w_range_f = (float) inv_w_range;

    // Convert precision if required.
    void *p_quadrature_kernel, *p_quadrature_nodes, *p_quadrature_weights;
    void *p_conv_corr_kernel;
    int mem_type;
    if (dbl_vis)
    {
        mem_type = MEM_DOUBLE;
        p_quadrature_kernel = quadrature_kernel;
        p_quadrature_nodes = quadrature_nodes;
        p_quadrature_weights = quadrature_weights;
        p_conv_corr_kernel = conv_corr_kernel;
    }
    else
    {
        // Cast to float.
        mem_type = MEM_FLOAT;
        p_quadrature_kernel  = calloc(QUADRATURE_SUPPORT_BOUND, sizeof(float));
        p_quadrature_nodes   = calloc(QUADRATURE_SUPPORT_BOUND, sizeof(float));
        p_quadrature_weights = calloc(QUADRATURE_SUPPORT_BOUND, sizeof(float));
        p_conv_corr_kernel   = calloc(image_size / 2 + 1, sizeof(float));
        for (int i = 0; i < QUADRATURE_SUPPORT_BOUND; ++i)
        {
            ((float*)p_quadrature_kernel)[i] = (float)(quadrature_kernel[i]);
            ((float*)p_quadrature_nodes)[i] = (float)(quadrature_nodes[i]);
            ((float*)p_quadrature_weights)[i] = (float)(quadrature_weights[i]);
        }
        for (int i = 0; i < image_size / 2 + 1; ++i)
            ((float*)p_conv_corr_kernel)[i] = (float)(conv_corr_kernel[i]);
        free(quadrature_kernel);
        free(quadrature_nodes);
        free(quadrature_weights);
        free(conv_corr_kernel);
    }
    Mem* h_quadrature_kernel = mem_create_alias_from_raw(p_quadrature_kernel,
            mem_type, MEM_CPU, QUADRATURE_SUPPORT_BOUND, status);
    Mem* h_quadrature_nodes = mem_create_alias_from_raw(p_quadrature_nodes,
            mem_type, MEM_CPU, QUADRATURE_SUPPORT_BOUND, status);
    Mem* h_quadrature_weights = mem_create_alias_from_raw(p_quadrature_weights,
            mem_type, MEM_CPU, QUADRATURE_SUPPORT_BOUND, status);
    Mem* h_conv_corr_kernel = mem_create_alias_from_raw(p_conv_corr_kernel,
            mem_type, MEM_CPU, image_size / 2 + 1, status);

    // Copy arrays to GPU.
    Mem* d_quadrature_kernel = wrapper.mem_create_copy(h_quadrature_kernel, MEM_GPU, status);
    Mem* d_quadrature_nodes = wrapper.mem_create_copy(h_quadrature_nodes, MEM_GPU, status);
    Mem* d_quadrature_weights = wrapper.mem_create_copy(h_quadrature_weights, MEM_GPU, status);
    Mem* d_conv_corr_kernel = wrapper.mem_create_copy(h_conv_corr_kernel, MEM_GPU, status);

    // Perform convolution correction and final scaling on single real plane
    // note: can recycle same block/thread dims as w correction kernel
    {
        const char* k = dbl_vis ?
                "conv_corr_and_scaling<double>" :
                "conv_corr_and_scaling<float>";
        num_threads[0] = std::min(32, (npix_x + 1) / 2);
        num_threads[1] = std::min(32, (npix_y + 1) / 2);
        // Allow extra in negative x quadrants, for asymmetric image centre
        num_blocks[0] = (npix_x / 2 + 1 + num_threads[0] - 1) / num_threads[0];
        num_blocks[1] = (npix_y / 2 + 1 + num_threads[1] - 1) / num_threads[1];
		const bool solving = true;
		const bool do_wstacking = false;
        const void* args[] = {
            mem_buffer(d_dirty_image),
            &image_size,
            dbl_vis ? (const void*)&pixel_size : (const void*)&pixel_size_f,
            &support,
            dbl_vis ?
                (const void*)&conv_corr_norm_factor :
                (const void*)&conv_corr_norm_factor_f,
            mem_buffer_const(d_conv_corr_kernel),
            dbl_vis ? (const void*)&inv_w_range : (const void*)&inv_w_range_f,
            dbl_vis ? (const void*)&inv_w_scale : (const void*)&inv_w_scale_f,
            mem_buffer_const(d_quadrature_kernel),
            mem_buffer_const(d_quadrature_nodes),
            mem_buffer_const(d_quadrature_weights),
			&solving,
			&do_wstacking
        };
        wrapper.launch_kernel(k, num_blocks, num_threads, 0, 0, args, status);
    }

    // Copy the image back to the host and free the device copy.
    wrapper.mem_copy_contents(im, d_dirty_image, 0, 0, npix_x * npix_y, status);
    wrapper.mem_free(d_dirty_image, status);

    // Free arrays used for convolution correction.
    free(p_quadrature_kernel);
    free(p_quadrature_nodes);
    free(p_quadrature_weights);
    free(p_conv_corr_kernel);
    wrapper.mem_free(h_quadrature_kernel, status);
    wrapper.mem_free(h_quadrature_nodes, status);
    wrapper.mem_free(h_quadrature_weights, status);
    wrapper.mem_free(h_conv_corr_kernel, status);
    wrapper.mem_free(d_quadrature_kernel, status);
    wrapper.mem_free(d_quadrature_nodes, status);
    wrapper.mem_free(d_quadrature_weights, status);
    wrapper.mem_free(d_conv_corr_kernel, status);
	
	LogF(verbosityLevel, 2, "\nEnd of ms2dirty_2d()... \n\n");
}
