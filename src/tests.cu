
// Copyright 2021 Adam Campbell, Anthony Griffin, Andrew Ensor, Seth Hall
// Copyright 2021 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.


#include "controller.h"
#include "tests.h"

#define NOT_PYTHON 1
#include "nifty_kernels.cuh"

#define PASS_TEXT "\033[1;32mPASS\033[0m"
#define FAIL_TEXT "\033[0;31mFAIL\033[0m"

#if NIFTY_PRECISION == NIFTY_SINGLE
	PRECISION passThreshold = 1e-6;
#else
	PRECISION passThreshold = 5e-14;
#endif

// Use of constant buffers instead of cudaMalloc'd memory allows for
// more efficient caching, and "broadcasting" across threads
extern __constant__ PRECISION quadrature_nodes[QUADRATURE_SUPPORT_BOUND];
extern __constant__ PRECISION quadrature_weights[QUADRATURE_SUPPORT_BOUND];
extern __constant__ PRECISION quadrature_kernel[QUADRATURE_SUPPORT_BOUND];

template<typename T>
T RRMSE(T *est, T *val, uint32_t N)
{
	T diffSum = 0;
	T  valSum = 0;
	for(uint32_t i = 0; i < N; i++)
	{
		T diff = val[i] - est[i];
		
		diffSum += diff*diff;
		
		valSum += val[i]*val[i] + 1;
	}
	
	//printf("diffSum is %+25.14e\n", SQRT(diffSum));
	//printf("valSum  is %+25.14e\n", SQRT(valSum));
	
	return (sqrt(diffSum)/sqrt(valSum));	
}

template<typename T, typename T2>
T RRMSE2(T2* est, T2* val, uint32_t N)
{
	T diffSum = 0;
	T  valSum = 0;
	for(uint32_t i = 0; i < N; i++)
	{
		T diff_re = val[i].x - est[i].x;
		T diff_im = val[i].y - est[i].y;
		
		diffSum  += diff_re*diff_re + diff_im*diff_im;
		
		valSum += val[i].x*val[i].x + val[i].y*val[i].y;
	}
	
	//printf("diffSum is %+25.14e\n", SQRT(diffSum));
	//printf("valSum  is %+25.14e\n", SQRT(valSum));
	
	return (sqrt(diffSum)/sqrt(valSum));	
}

int test_imaging_pipeline(config *config)
{
    host_memory_handles host;
    device_memory_handles device;
    init_memory_handles(&host, &device);

	// prepare for HIPPO dataset
	setup_for_HIPPO_data(config);

    // Allocate host based memory
    allocate_host_bound_buffers(&host, &device, config);

    // Populate vis from file
    load_visibilities_from_file(&host, config);

    // Calculate gauss-legendre quadrature values for convolution correction
    generate_gauss_legendre_conv_kernel(&host, config);
	
    // Run full system (gridding and degridding) stages
	LogF(config, 0, "\nSystem tests:\n");
    run_system_test(&host, &device, config);
	
	// Check gridding output
	check_dirty_image(&host, &device, config);
	// Check degridding output
	check_predicted_visibilities(&host, &device, config);

	// unit tests
	// LogF(config, 0, "\nUnit tests:\n");
	// test_conv_corr_and_scaling(&host, &device, config);
	// test_nifty_gridding(       &host, &device, config);
	
    // Clean up
	LogF(config, 0, "\n");
	LogF(config, 3, "Cleaning up allocated buffers.\n");
    nifty_cleanup(&device);
    clean_up_memory_handles(&host, &device);
    cudaDeviceReset();

    return EXIT_SUCCESS;
}

void test_nifty_gridding(host_memory_handles *host, device_memory_handles *device, config *config)
{
	PRECISION error = 0;

    // Populate vis from file
    load_visibilities_from_file(host, config);

	// this ensures that the next call copies over the visibilities
    if(device->d_visibilities) 
        CUDA_CHECK_RETURN(cudaFree(device->d_visibilities));
    device->d_visibilities = NULL;

    // Perform set up (ie: allocate memory, copy over data, etc.)
    nifty_init(host, device, config);

    // Allow CUDA to determine block/thread distribution
    // following https://developer.nvidia.com/blog/cuda-pro-tip-occupancy-api-simplifies-launch-configuration/
    int min_cuda_grid_size;
    int cuda_block_size;
    cudaOccupancyMaxPotentialBlockSize(&min_cuda_grid_size, &cuda_block_size, nifty_gridding<VIS_PRECISION, VIS_PRECISION2, PRECISION, PRECISION2, PRECISION3>, 0, 0);
    int cuda_grid_size = (config->num_visibilities + cuda_block_size - 1) / cuda_block_size;  // create 1 thread per visibility to be gridded
    LogF(config, 3, "Gridder using grid size %i, where minimum grid size available is %i, and using block size %i.\n", 
		cuda_grid_size, min_cuda_grid_size, cuda_block_size);
    LogF(config, 3, "Gridder calling nifty_gridding kernel.\n");

    // Determine how many w grid subset batches to process in total
    uint32_t total_w_grid_batches = (config->num_total_w_grids + config->num_w_grids_batched - 1) / config->num_w_grids_batched;
    //uint32_t total_w_grid_batches = 1;
    uint32_t num_w_grid_stack_cells = config->grid_size * config->grid_size * config->num_w_grids_batched;

	CUDA_CHECK_RETURN(cudaMemset(device->d_w_grid_stack, 0, num_w_grid_stack_cells * sizeof(PRECISION2)));
	CUDA_CHECK_RETURN( cudaGetLastError() );
	CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

	bool bGeneratingTestCode = true;

	for(int batch = 0; batch < total_w_grid_batches; batch++)
	{
		if (bGeneratingTestCode)
		{
			CUDA_CHECK_RETURN(cudaMemset(device->d_w_grid_stack, 0, num_w_grid_stack_cells * sizeof(PRECISION2)));
			CUDA_CHECK_RETURN( cudaGetLastError() );
			CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
		}
		
		uint32_t num_w_grids_subset = min(
			config->num_w_grids_batched,
			config->num_total_w_grids - ((batch * config->num_w_grids_batched) % config->num_total_w_grids)
		);

		int32_t grid_start_w = batch*config->num_w_grids_batched;

		LogF(config, 2, "Gridder calling nifty_gridding kernel for w grids %i to %i (plane(s) in current batch).\n",
			grid_start_w, (grid_start_w + (int32_t)num_w_grids_subset - 1), num_w_grids_subset);
		
		// Perform gridding on a "chunk" of w grids
		nifty_gridding<VIS_PRECISION, VIS_PRECISION2, PRECISION, PRECISION2, PRECISION3><<<cuda_grid_size, cuda_block_size>>>(
			config->num_visibilities,
			1, // hard-coded number of channels for now!!!
			device->d_visibilities,
			device->d_vis_weights,
			device->d_uvw_coords,
			device->d_w_grid_stack,
			config->grid_size,
			grid_start_w,
			num_w_grids_subset,
			config->support,
			config->beta,
			//config->upsampling,
			config->uv_scale,
			config->w_scale, 
			config->min_plane_w,
			config->freq / PRECISION(C),
			config->generating_psf,
			(bGeneratingTestCode ? 0 :config->perform_shift_fft),
			true // gridding
		);
		CUDA_CHECK_RETURN( cudaGetLastError() );
		CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

		/**************************************************************************************
		 * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
		 *************************************************************************************/
		// // Copy back and save each w grid to file for review of accuracy
		// std::cout << "Copying back grid stack from device to host..." << std::endl; 
		//proof_of_concept_copy_w_grid_stack_to_host(host, device, config);
		// Save each w grid into single file of complex type, use python to render via matplotlib
		// std::cout << "Saving grid stack to file..." << std::endl;
		//proof_of_concept_save_w_grids_to_file(host, config, batch);
		/**************************************************************************************
		 * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
		 *************************************************************************************/
		
		if(0)
		{
			char grid_name_buffer[257];

			snprintf(grid_name_buffer, 257, "w_grid_%i", grid_start_w);
			
			PRECISION2* test_grid = load_test_grid(config, grid_name_buffer);

			error = RRMSE2<PRECISION, PRECISION2>(test_grid, host->w_grid_stack, config->grid_size*config->grid_size);

			LogF(config, 0, "    nifty_gridding         RRMSE is %e.  ", error);
			LogF(config, 0, "%s\n", ((error < passThreshold) ? PASS_TEXT : FAIL_TEXT) );

			free(test_grid);			 
		}
	}
}

void test_conv_corr_and_scaling(host_memory_handles *host, device_memory_handles *device, config *config)
{
	// load test
	PRECISION* test_dirty_image_preCC = load_test_dirty_image(config, "dirty_image_preCC");
	
	memcpy(host->dirty_image, test_dirty_image_preCC, config->image_size*config->image_size*sizeof(PRECISION));
	
	copy_dirty_image_to_device(host, device, config);

	free(test_dirty_image_preCC);
	
    // Determine the block/thread distribution per w correction and summation batch
    // note: based on image size, not grid size, as we take inner region and discard padding
    dim3 w_correction_threads(
        min((uint32_t)32, (config->image_size + 1) / 2),
        min((uint32_t)32, (config->image_size + 1) / 2)
    );
    dim3 w_correction_blocks(
        (config->image_size/2 + 1 + w_correction_threads.x - 1) / w_correction_threads.x,  // allow extra in negative x quadrants, for asymmetric image centre
        (config->image_size/2 + 1 + w_correction_threads.y - 1) / w_correction_threads.y   // allow extra in negative y quadrants, for asymmetric image centre
    );

	// Need to determine normalisation factor for scaling runtime calculated
	// conv correction values for coordinate n (where n = sqrt(1 - l^2 - m^2) - 1)
	uint32_t p = (uint32_t)(CEIL(PRECISION(1.5)*config->support + PRECISION(2.0)));
	PRECISION conv_corr_norm_factor = 0.0;
	for(uint32_t i = 0; i < p; i++)
		conv_corr_norm_factor += host->quadrature_kernel[i]*host->quadrature_weights[i];
	conv_corr_norm_factor *= (PRECISION)config->support;

	// Need to determine final scaling factor for scaling dirty image by w grid accumulation
	PRECISION inv_w_range = 1.0 / (config->max_plane_w - config->min_plane_w);

	// Need sum of visibility weights for scaling dirty image
	PRECISION vis_weights_sum = 0.0;
	for(uint32_t v = 0; v < config->num_visibilities; v++)
		vis_weights_sum += host->vis_weights[v];

	LogF(config, 3, "Sum of weights: %f\n", vis_weights_sum * config->num_channels);

	// Perform convolution correction and final scaling on single real plane
	// note: can recycle same block/thread dims as w correction kernel
	conv_corr_and_scaling<PRECISION><<<w_correction_blocks, w_correction_threads>>>(
		device->d_dirty_image,
		config->image_size,
		config->pixel_size,
		config->support,
		conv_corr_norm_factor,
		device->d_conv_corr_kernel,
		inv_w_range,
		vis_weights_sum * config->num_channels,
		1.0/config->w_scale,
    quadrature_kernel,
    quadrature_nodes,
    quadrature_weights,
		true // solving
	);

    // Copy back image
    copy_dirty_image_to_host(host, device, config);

	PRECISION error = 0;

	PRECISION* test_dirty_image = load_test_dirty_image(config, "dirty_image");

	error = RRMSE<PRECISION>(test_dirty_image, host->dirty_image, config->image_size*config->image_size);

    LogF(config, 0, "    conv_corr_and_scaling  RRMSE is %e.  ", error);

	LogF(config, 0, "%s\n", ((error < passThreshold) ? PASS_TEXT : FAIL_TEXT) );

    free(test_dirty_image);
}

void run_system_test(host_memory_handles *host, device_memory_handles *device, config *config)
{
    // Perform set up (ie: allocate memory, copy over data, etc.)
    nifty_init(host, device, config);

    // Allow CUDA to determine block/thread distribution
    // following https://developer.nvidia.com/blog/cuda-pro-tip-occupancy-api-simplifies-launch-configuration/
    int min_cuda_grid_size;
    int cuda_block_size;
    cudaOccupancyMaxPotentialBlockSize(&min_cuda_grid_size, &cuda_block_size, nifty_gridding<VIS_PRECISION, VIS_PRECISION2, PRECISION, PRECISION2, PRECISION3>, 0, 0);
    int cuda_grid_size = (config->num_visibilities + cuda_block_size - 1) / cuda_block_size;  // create 1 thread per visibility to be gridded
    LogF(config, 1, "Gridder using grid size %i, where minimum grid size available is %i, and using block size %i.\n", 
		cuda_grid_size, min_cuda_grid_size, cuda_block_size);
    LogF(config, 1, "Gridder calling nifty_gridding kernel.\n");

    // Determine the block/thread distribution per w correction and summation batch
    // note: based on image size, not grid size, as we take inner region and discard padding
    dim3 w_correction_threads(
        min((uint32_t)32, (config->image_size + 1) / 2),
        min((uint32_t)32, (config->image_size + 1) / 2)
    );
    dim3 w_correction_blocks(
        (config->image_size/2 + 1 + w_correction_threads.x - 1) / w_correction_threads.x,  // allow extra in negative x quadrants, for asymmetric image centre
        (config->image_size/2 + 1 + w_correction_threads.y - 1) / w_correction_threads.y   // allow extra in negative y quadrants, for asymmetric image centre
    );

    // Determine how many w grid subset batches to process in total
    uint32_t total_w_grid_batches = (config->num_total_w_grids + config->num_w_grids_batched - 1) / config->num_w_grids_batched;
    uint32_t num_w_grid_stack_cells = config->grid_size * config->grid_size * config->num_w_grids_batched;

    dim3 scalar_threads(
        min((uint32_t)32, config->grid_size),
        min((uint32_t)32, config->grid_size)
    );
    dim3 scalar_blocks(
        (int)ceil((double)config->grid_size / scalar_threads.x), 
        (int)ceil((double)config->grid_size / scalar_threads.y)
    );

    // // Simulate major cycles (ie: perform gridding on predicted visibilities)
    for(int cycle = 0; cycle < 1; cycle++)
    {
        uint32_t num_dirty_image_pixels = config->image_size * config->image_size;
        CUDA_CHECK_RETURN(cudaMemset(device->d_dirty_image, 0, num_dirty_image_pixels * sizeof(PRECISION)));
        CUDA_CHECK_RETURN( cudaGetLastError() );
        CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
        CUDA_CHECK_RETURN(cudaMemset(device->d_w_grid_stack, 0, num_w_grid_stack_cells * sizeof(PRECISION2)));
        CUDA_CHECK_RETURN( cudaGetLastError() );
        CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

        for(int batch = 0; batch < total_w_grid_batches; batch++)
        {
            uint32_t num_w_grids_subset = min(
                config->num_w_grids_batched,
                config->num_total_w_grids - ((batch * config->num_w_grids_batched) % config->num_total_w_grids)
            );

            int32_t grid_start_w = batch*config->num_w_grids_batched;

			LogF(config, 2, "Gridder calling nifty_gridding kernel for w grids %i to %i (plane(s) in current batch).\n",
				grid_start_w, (grid_start_w + (int32_t)num_w_grids_subset - 1), num_w_grids_subset);
            
            // Perform gridding on a "chunk" of w grids
            nifty_gridding<VIS_PRECISION, VIS_PRECISION2, PRECISION, PRECISION2, PRECISION3><<<cuda_grid_size, cuda_block_size>>>(
                config->num_visibilities,
				1, // hard-coded number of channels for now!!!
                device->d_visibilities,
                device->d_vis_weights,
                device->d_uvw_coords,
                device->d_w_grid_stack,
                config->grid_size,
                grid_start_w,
                num_w_grids_subset,
                config->support,
                config->beta,
                //config->upsampling,
                config->uv_scale,
                config->w_scale, 
                config->min_plane_w,
				config->freq / PRECISION(C),
                config->generating_psf,
                config->perform_shift_fft,
                true // gridding
            );
            CUDA_CHECK_RETURN( cudaGetLastError() );
            CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

            /**************************************************************************************
             * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
             *************************************************************************************/
            // // Copy back and save each w grid to file for review of accuracy
            // std::cout << "Copying back grid stack from device to host..." << std::endl; 
            //proof_of_concept_copy_w_grid_stack_to_host(host, device, config);
            // Save each w grid into single file of complex type, use python to render via matplotlib
            // std::cout << "Saving grid stack to file..." << std::endl;
            //proof_of_concept_save_w_grids_to_file(host, config, batch);
            /**************************************************************************************
             * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
             *************************************************************************************/

            // Perform 2D FFT on each bound w grid
            CUFFT_SAFE_CALL(CUFFT_EXECUTE_C2C(*(device->fft_plan), device->d_w_grid_stack, device->d_w_grid_stack, CUFFT_INVERSE));
            CUDA_CHECK_RETURN( cudaGetLastError() );
            CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

            // if(cycle == 0)
            // {
            //     scale_for_FFT<<<scalar_blocks, scalar_threads>>>(
            //         device->d_w_grid_stack, 
            //         num_w_grids_subset, 
            //         config->grid_size, 
            //         1.0/(config->grid_size*config->grid_size)
            //     );
            // }
            
            /**************************************************************************************
             * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
             *************************************************************************************/
            // // Copy back and save each w grid to file for review of accuracy
            // std::cout << "Copying back grid stack from device to host..." << std::endl; 
            //proof_of_concept_copy_w_grid_stack_to_host(host, device, config);
            // // Save each w grid into single file of complex type, use python to render via matplotlib
            // std::cout << "Saving grid stack to file..." << std::endl;
            //proof_of_concept_save_w_images_to_file(host, config, batch);
            /**************************************************************************************
             * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
             *************************************************************************************/
            
    		// Perform phase shift on a "chunk" of planes and sum into single real plane
            apply_w_screen_and_sum<PRECISION, PRECISION2><<<w_correction_blocks, w_correction_threads>>>(
                device->d_dirty_image,
                config->image_size,
                config->pixel_size,
                device->d_w_grid_stack,
                config->grid_size,
                grid_start_w,
                num_w_grids_subset,
                PRECISION(1.0)/config->w_scale,
                config->min_plane_w,
                config->perform_shift_fft
            );
            CUDA_CHECK_RETURN( cudaGetLastError() );
            CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

            // Reset intermediate w grid buffer for next batch
            if(batch < total_w_grid_batches - 1)
            {
                LogF(config, 3, "Resetting device w grid stack memory for next batch...\n");
                CUDA_CHECK_RETURN(cudaMemset(device->d_w_grid_stack, 0, num_w_grid_stack_cells * sizeof(PRECISION2)));
                CUDA_CHECK_RETURN( cudaGetLastError() );
                CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
            }
        }

        //copy_dirty_image_to_host(host, device, config);
        //save_image2_to_file(host, config, cycle);

        // Need to determine normalisation factor for scaling runtime calculated
        // conv correction values for coordinate n (where n = sqrt(1 - l^2 - m^2) - 1)
        uint32_t p = (uint32_t)(CEIL(PRECISION(1.5)*config->support + PRECISION(2.0)));
        PRECISION conv_corr_norm_factor = 0.0;
        for(uint32_t i = 0; i < p; i++)
            conv_corr_norm_factor += host->quadrature_kernel[i]*host->quadrature_weights[i];
        conv_corr_norm_factor *= (PRECISION)config->support;

        // Need to determine final scaling factor for scaling dirty image by w grid accumulation
        PRECISION inv_w_range = 1.0 / (config->max_plane_w - config->min_plane_w);

        // Need sum of visibility weights for scaling dirty image
        PRECISION vis_weights_sum = 0.0;
        for(uint32_t v = 0; v < config->num_visibilities; v++)
            vis_weights_sum += host->vis_weights[v];

        LogF(config, 3, "Sum of weights: %f\n", vis_weights_sum * config->num_channels);

        // Perform convolution correction and final scaling on single real plane
        // note: can recycle same block/thread dims as w correction kernel
        conv_corr_and_scaling<PRECISION><<<w_correction_blocks, w_correction_threads>>>(
            device->d_dirty_image,
            config->image_size,
            config->pixel_size,
            config->support,
            conv_corr_norm_factor,
            device->d_conv_corr_kernel,
            inv_w_range,
            vis_weights_sum * config->num_channels,
            PRECISION(1.0)/config->w_scale,
    quadrature_kernel,
    quadrature_nodes,
    quadrature_weights,
            true // solving
        );
        CUDA_CHECK_RETURN( cudaGetLastError() );
        CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

        // Copy back images, save to file
        copy_dirty_image_to_host(host, device, config);
        save_image_to_file(host, config, cycle);

        if(cycle == 1)
        {
            printf("EARLY EXIT FOR PREDICTION...\n");
            exit(0);
        }

        // Perform degridding here..
        CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
        CUDA_CHECK_RETURN(cudaMemset(device->d_visibilities, 0, sizeof(VIS_PRECISION2) * config->num_visibilities));
        CUDA_CHECK_RETURN( cudaGetLastError() );
        CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
        CUDA_CHECK_RETURN(cudaMemset(device->d_w_grid_stack, 0, num_w_grid_stack_cells * sizeof(PRECISION2)));
        CUDA_CHECK_RETURN( cudaGetLastError() );
        CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

        // 1. Undo convolution correction and scaling
        conv_corr_and_scaling<PRECISION><<<w_correction_blocks, w_correction_threads>>>(
            device->d_dirty_image,
            config->image_size,
            config->pixel_size,
            config->support,
            conv_corr_norm_factor,
            device->d_conv_corr_kernel,
            inv_w_range,
            vis_weights_sum * config->num_channels,
            PRECISION(1.0)/config->w_scale,
    quadrature_kernel,
    quadrature_nodes,
    quadrature_weights,
            false // predicting
        );
        CUDA_CHECK_RETURN( cudaGetLastError() );
        CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

        // Copy back images, save to file
        //copy_dirty_image_to_host(host, device, config);
        //save_image2_to_file(host, config, cycle);

        for(int batch = 0; batch < total_w_grid_batches; batch++)
        {
            uint32_t num_w_grids_subset = min(
                config->num_w_grids_batched,
                config->num_total_w_grids - ((batch * config->num_w_grids_batched) % config->num_total_w_grids)
            );

            int32_t grid_start_w = batch*config->num_w_grids_batched;

			LogF(config, 3, "Reversing...\n");

            // 2. Undo w-stacking and dirty image accumulation
            reverse_w_screen_to_stack<PRECISION, PRECISION2><<<w_correction_blocks, w_correction_threads>>>(
                device->d_dirty_image,
                config->image_size,
                config->pixel_size,
                device->d_w_grid_stack,
                config->grid_size,
                grid_start_w,
                num_w_grids_subset,
                PRECISION(1.0)/config->w_scale,
                config->min_plane_w,
                config->perform_shift_fft
            );

            CUDA_CHECK_RETURN( cudaGetLastError() );
            CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

            LogF(config, 3, "Reversing FINISHED...\n");

            /**************************************************************************************
             * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
             *************************************************************************************/
            // Copy back and save each w grid to file for review of accuracy
            // std::cout << "Copying back grid stack from device to host..." << std::endl; 
            // proof_of_concept_copy_w_grid_stack_to_host(host, device, config);
            // // Save each w grid into single file of complex type, use python to render via matplotlib
            // std::cout << "Saving grid stack to file..." << std::endl;
            // proof_of_concept_save_w_images_to_file(host, config, batch);
            /**************************************************************************************
             * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
             *************************************************************************************/

            // 3. Batch FFT
            CUFFT_SAFE_CALL(CUFFT_EXECUTE_C2C(*(device->fft_plan), device->d_w_grid_stack, device->d_w_grid_stack, CUFFT_FORWARD));
            CUDA_CHECK_RETURN( cudaGetLastError() );
            CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

            //  scale_for_FFT<<<scalar_blocks, scalar_threads>>>(
            //     device->d_w_grid_stack, 
            //     num_w_grids_subset, 
            //     config->grid_size, 
            //     1.0/(config->grid_size*config->grid_size)
            // );


            /**************************************************************************************
             * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
             *************************************************************************************/
            // // Copy back and save each w grid to file for review of accuracy
            // std::cout << "Copying back grid stack from device to host..." << std::endl; 
            // proof_of_concept_copy_w_grid_stack_to_host(host, device, config);
            // // Save each w grid into single file of complex type, use python to render via matplotlib
            // std::cout << "Saving grid stack to file..." << std::endl;
            // proof_of_concept_save_w_grids_to_file(host, config, batch);
            /**************************************************************************************
             * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
             *************************************************************************************/

            // if(grid_start_w == 3)
            // {
                // 4. Degrid visibilities from each w-grid stack
                nifty_gridding<VIS_PRECISION, VIS_PRECISION2, PRECISION, PRECISION2, PRECISION3><<<cuda_grid_size, cuda_block_size>>>(
					config->num_visibilities,
					1, // hard-coded number of channels for now!!!
                    device->d_visibilities,
                    device->d_vis_weights,
                    device->d_uvw_coords,
                    device->d_w_grid_stack,
                    config->grid_size,
                    grid_start_w,
                    num_w_grids_subset,
                    config->support,
                    config->beta,
                    //config->upsampling,
                    config->uv_scale,
                    config->w_scale, 
                    config->min_plane_w,
					config->freq / PRECISION(C),
                    config->generating_psf,
                    config->perform_shift_fft,
                    false // degridding
                );
                CUDA_CHECK_RETURN( cudaGetLastError() );
                CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
            // }

            
            // Reset intermediate w grid buffer for next batch
            if(batch < total_w_grid_batches - 1)
            {
                LogF(config, 3, "Resetting device w grid stack memory for next batch...\n");
                CUDA_CHECK_RETURN(cudaMemset(device->d_w_grid_stack, 0, num_w_grid_stack_cells * sizeof(PRECISION2)));
                CUDA_CHECK_RETURN( cudaGetLastError() );
                CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
            }
        }
        CUDA_CHECK_RETURN(cudaMemcpy(host->visibilities, device->d_visibilities, sizeof(VIS_PRECISION2) * config->num_visibilities, cudaMemcpyDeviceToHost));
        save_predicted_visibilities(host, config);
    }

    // Copy back predicted vis from device, send to file...
    //CUDA_CHECK_RETURN(cudaMemcpy(host->visibilities, device->d_visibilities, sizeof(VIS_PRECISION2) * config->num_visibilities, cudaMemcpyDeviceToHost));
    //save_predicted_visibilities(host, config);


    // Finish
}

PRECISION check_dirty_image(host_memory_handles *host, device_memory_handles *device, config *config)
{
	PRECISION error = 0;

	PRECISION* test_dirty_image = load_test_dirty_image(config, "dirty_image");
	
	error = RRMSE<PRECISION>(test_dirty_image, host->dirty_image, config->image_size*config->image_size);

    LogF(config, 0, "    Dirty image            RRMSE is %e.  ", error);

	LogF(config, 0, "%s\n", ((error < passThreshold) ? PASS_TEXT : FAIL_TEXT) );

    free(test_dirty_image);
	
	return(error);
}

PRECISION2* load_test_grid(config *config, const char* specifier)
{
    char file_name_buffer[257];

#if NIFTY_PRECISION == NIFTY_SINGLE
	char precision_char = 'S';
#else
	char precision_char = 'D';
#endif

	// NB: this path is hard-coded!!
    snprintf(file_name_buffer, 257, "/home/seth/Desktop/Anthony/CUDA_Nifty/test_data/test_%s_%i_%c.bin", 
		specifier, config->grid_size, precision_char);
	
	FILE *grid_file = fopen(file_name_buffer, "rb");

    if(grid_file == NULL)
    {
        LogF(config, 0, "ERR: Unable to locate test grid file [%s].\n", file_name_buffer);
        exit(EXIT_FAILURE);
    }

	uint32_t num_cells = config->grid_size*config->grid_size;

    PRECISION2* temp_grid = (PRECISION2*) calloc(num_cells, sizeof(PRECISION2));
    uint32_t num_cells_found = fread(temp_grid, sizeof(PRECISION2), num_cells, grid_file);

    fclose(grid_file);

    if(num_cells_found != num_cells)
    {
        LogF(config, 0, "ERR: Number of grid cells read from file does not match NIFTy config.\n");
        LogF(config, 0, "%i vs %i\n", num_cells_found, num_cells);
        exit(EXIT_FAILURE);
    }

	return(temp_grid);
}

PRECISION* load_test_dirty_image(config *config, const char* specifier)
{
    char file_name_buffer[257];

#if NIFTY_PRECISION == NIFTY_SINGLE
	char precision_char = 'S';
#else
	char precision_char = 'D';
#endif

	// NB: this path is hard-coded!!
    snprintf(file_name_buffer, 257, "/home/seth/Desktop/Anthony/CUDA_Nifty/test_data/test_%s_%i_%c.bin", 
		specifier, config->image_size, precision_char);
	
	FILE *dirty_image_file = fopen(file_name_buffer, "rb");

    if(dirty_image_file == NULL)
    {
        LogF(config, 0, "ERR: Unable to locate test dirty image file.\n");
        exit(EXIT_FAILURE);
    }

	uint32_t num_pixels = config->image_size*config->image_size;

    PRECISION* temp_dirty_image = (PRECISION*) calloc(num_pixels, sizeof(PRECISION));
    uint32_t num_pixels_found = fread(temp_dirty_image, sizeof(PRECISION), num_pixels, dirty_image_file);

    fclose(dirty_image_file);

    if(num_pixels_found != num_pixels)
    {
        LogF(config, 0, "ERR: Number of pixels read from file does not match NIFTy config.\n");
        LogF(config, 0, "%i vs %i\n", num_pixels_found, num_pixels);
        exit(EXIT_FAILURE);
    }

	return(temp_dirty_image);
}

PRECISION check_predicted_visibilities(host_memory_handles *host, device_memory_handles *device, config *config)
{
	PRECISION error = 0;

	VIS_PRECISION2* test_visibilities = load_predicted_visibilities_from_file(host, config);

	for(int32_t v = 0; v < 10; v++)
    {
		LogF(config, 2, "Est  Visibility %2i: (%+20.10e, %+20.10e)\n", v, host->visibilities[v].x, host->visibilities[v].y);
		LogF(config, 2, "Test Visibility %2i: (%+20.10e, %+20.10e)\n", v,  test_visibilities[v].x,  test_visibilities[v].y);
    }
	
	error = RRMSE2<PRECISION, VIS_PRECISION2>(test_visibilities, host->visibilities, config->num_visibilities);

    LogF(config, 0, "    Predicted visibilities RRMSE is %e.  ", error);

	LogF(config, 0, "%s\n", ((error < passThreshold) ? PASS_TEXT : FAIL_TEXT) );
	
    free(test_visibilities);
	
	return(error);
}

PRECISION2* load_predicted_visibilities_from_file(host_memory_handles *host, config *config)
{
	// NB: these names are hard-coded!!
#if VISIBILITY_PRECISION == NIFTY_SINGLE
    FILE *vis_intensities_file = 
fopen("/home/seth/Desktop/Anthony/CUDA_Nifty/test_data/test_predicted_vis_1024_S.bin", "rb");
#else
    FILE *vis_intensities_file = 
fopen("/home/seth/Desktop/Anthony/CUDA_Nifty/test_data/test_predicted_vis_1024_D.bin", "rb");
#endif

    if(vis_intensities_file == NULL)
    {
        LogF(config, 0, "ERR: Unable to locate predicted visibility files.\n");
        exit(EXIT_FAILURE);
    }

    PRECISION2* temp_intensities = (PRECISION2*) calloc(config->num_visibilities, sizeof(PRECISION2));
    uint32_t num_intensities_found = fread(temp_intensities, sizeof(PRECISION2), config->num_visibilities, vis_intensities_file);

    fclose(vis_intensities_file);

    if(num_intensities_found != config->num_visibilities)
    {
        LogF(config, 0, "ERR: Number of visibilities read from file does not match NIFTy config.\n");
        exit(EXIT_FAILURE);
    }

	//config->num_visibilities = 10;

    //for(int v = 0; v < config->num_visibilities; v++)
    //{
		//LogF(config, 99, "Visibility %2i: (%+12e, %+12e)\n", v, temp_intensities[v].x, temp_intensities[v].y);
    //}
	
	return(temp_intensities);
}

void setup_for_HIPPO_data(config *config)
{
    // Define where output files to be saved, ie: dirty image, intermediate w grids (testing only)
    strcpy(config->data_output_folder, "/home/seth/Desktop/Anthony/CUDA_Nifty/ska-gridder-nifty-cuda/output");

    strcpy(config->vis_intensity_file, "/home/seth/Desktop/Anthony/CUDA_Nifty/data/HIPPO_vis");

    strcpy(config->vis_uvw_file, "/home/seth/Desktop/Anthony/CUDA_Nifty/data/HIPPO_uvw");

    config->generating_psf = false; // uses nifty gridder with visibilities of 1.0+0.0i

    config->perform_shift_fft = true; // flag to (equivalently) rearrange each grid so origin is at lower-left corner for FFT

    config->upsampling = 2.0; // sigma, the scaling factor for padding each w grid (image_size * upsampling)

    config->image_size = 1024*1; // number of pixels in one dimension for dirty image

    config->grid_size = FLOOR(config->image_size * config->upsampling); // upscaled one dimension size of grid for each w grid

    config->num_visibilities = 175500; // How many visibilities to process
    //config->num_visibilities = 2; // How many visibilities to process

    config->support = 7; // full support for semicircle gridding kernel

    config->beta = (VIS_PRECISION) (2.307 * ((PRECISION)config->support)); // NOTE this 2.307 is only for when upsampling = 2

    config->fov = 20.0; // field of view in degrees

    config->freq = 1e8; // frequency of sampled visibility data (currently single chan only)

    config->num_channels = 1; // number of total channels (currently single chan only)

    // converts pixel index (x, y) to normalised image coordinate (l, m) where l, m between -0.5 and 0.5
    config->pixel_size = asin(2.0 * sin(0.5 * config->fov*PI/(180.0)) / PRECISION(config->image_size));

    // scaling factor for conversion of uv coords to grid coordinates (grid_size * cell_size)
    config->uv_scale = config->pixel_size * (PRECISION)config->grid_size * config->freq / C;

	switch(config->num_visibilities)
	{
		case 175500:
			config->min_abs_w = 9.3730071057507303e-05;
			config->max_abs_w = 6.0272695994662467e+02;   // with 175500 visibilities
			break;
		case 2:
			config->min_abs_w = 2.1497790053524568e+01;
			config->max_abs_w = 4.6325368975632500e+01;
			break;
		case 5:
			config->min_abs_w = 2.149779005352e+01;
			config->max_abs_w = 1.596495948488e+02;
			break;
		case 10:
			config->min_abs_w = 3.143103496678e+00;
			config->max_abs_w = 3.231917769488e+02;
			break;
		case 20:
			config->min_abs_w = 3.143103496678e+00;
			config->max_abs_w = 3.231917769488e+02;
			break;
		case 40:
			config->min_abs_w = 3.143103496678e+00;
			config->max_abs_w = 3.231917769488e+02;
			break;
		case 60:
			config->min_abs_w = 3.143103496678e+00;
			config->max_abs_w = 3.231917769488e+02;
			break;
		case 100:
			config->min_abs_w = 3.143103496678e+00;
			config->max_abs_w = 3.574805404221e+02;
			break;
		case 1000:
			config->min_abs_w = 4.019969699916e-01;
			config->max_abs_w = 6.027269599466e+02;
			break;
		case 10000:
			config->min_abs_w = 2.655283906097e-02;
			config->max_abs_w = 6.027269599466e+02;
			break;
		case 100000:
			config->min_abs_w = 9.373007105751e-05;
			config->max_abs_w = 6.027269599466e+02;
			break;
		default:
			LogF(config, 0, "ERROR: config->num_visibilities not recognised!!!\n");
			// should exit here??
	}
	
    LogF(config, 2, "Min w: %e\n", config->min_abs_w);
    LogF(config, 2, "Max w: %e\n", config->max_abs_w);
	
    config->num_w_grids_batched = 1; // How many w grids to grid at once on the GPU

    /****************************************************************************************
     * DO NOT MODIFY BELOW - DO NOT MODIFY BELOW - DO NOT MODIFY BELOW - DO NOT MODIFY BELOW 
     ***************************************************************************************/
    config->min_abs_w *= config->freq / C; // scaled from meters to wavelengths
    config->max_abs_w *= config->freq / C; // scaled from meters to wavelengths
    double x0 = -0.5 * config->image_size * config->pixel_size;
    double y0 = -0.5 * config->image_size * config->pixel_size;
    double nmin = sqrt(MAX(1.0 - x0*x0 - y0*y0, 0.0)) - 1.0;
	
    if (x0*x0 + y0*y0 > 1.0)
        nmin = -sqrt(abs(1.0 - x0*x0 - y0*y0)) - 1.0;

	LogF(config, 9, "nmin: %.16f\n",  nmin);
    config->w_scale = 0.25 / abs(nmin); // scaling factor for converting w coord to signed w grid index
	LogF(config, 9, "w_scale: %.16f\n",  config->w_scale);
    config->num_total_w_grids = (uint32_t) ( (config->max_abs_w - config->min_abs_w) / config->w_scale + 2); //  number of w grids required
    //config->inv_w_scale = (1.0 + 1e-13) * (config->max_abs_w - config->min_abs_w) / (config->num_total_w_grids - 1);
    config->w_scale = 1.0 / ((1.0 + 1e-13) * (config->max_abs_w - config->min_abs_w) / (config->num_total_w_grids - 1));
    config->min_plane_w = config->min_abs_w - (0.5*(PRECISION)config->support-1.0) / config->w_scale;
    config->max_plane_w = config->max_abs_w + (0.5*(PRECISION)config->support-1.0) / config->w_scale;
    config->num_total_w_grids += config->support - 2;
    /****************************************************************************************
     * DO NOT MODIFY ABOVE - DO NOT MODIFY ABOVE - DO NOT MODIFY ABOVE - DO NOT MODIFY ABOVE 
     ***************************************************************************************/

    LogF(config, 3, "Min w: %e\n",  	 config->min_abs_w);
    LogF(config, 2, "Max w: %e\n", 		 config->max_abs_w);
    LogF(config, 2, "Min w plane: %e\n", config->min_plane_w);
    LogF(config, 2, "Max w plane: %e\n", config->max_plane_w);
    //LogF(config, 2, "inv W scale: %.16f\n",  config->inv_w_scale);
    LogF(config, 2, "W scale: %.16f\n",  config->w_scale);
    LogF(config, 2, "Num planes: %d\n",  config->num_total_w_grids);
    LogF(config, 2, "Pixel size: %e\n",  config->pixel_size);
    LogF(config, 2, "UV scale: %e\n ",   config->uv_scale);
}
