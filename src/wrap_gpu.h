/* See the LICENSE file at the top-level directory of this distribution. */

#ifndef WRAP_GPU_H_
#define WRAP_GPU_H_

#include <map>
#include <string>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct Mem Mem;
typedef struct FFT FFT;

enum MEM_TYPE
{
    MEM_CHAR = 0x01,
    MEM_INT = 0x02,
    MEM_FLOAT = 0x04,
    MEM_DOUBLE = 0x08,
    MEM_COMPLEX = 0x20,
    MEM_COMPLEX_FLOAT = MEM_FLOAT | MEM_COMPLEX,
    MEM_COMPLEX_DOUBLE = MEM_DOUBLE | MEM_COMPLEX,
};

enum MEM_LOCATION
{
    MEM_CPU = 0,
    MEM_GPU = 1,
};

enum WRAP_GPU_ERRORS
{
    ERR_GPU_NO_CUDA = 1,
    ERR_GPU_NO_CUFFT,
    ERR_GPU_NO_NVRTC,
    ERR_GPU_KERNEL_NOT_FOUND,
    ERR_GPU_KERNEL_LAUNCH_FAILURE,
    ERR_MEM_ALLOC_FAILURE,
    ERR_MEM_BAD_TYPE,
    ERR_MEM_BAD_LOCATION,
    ERR_MEM_COPY_FAILURE,
};

enum FFT_TYPE
{
    FFT_C2C,
    FFT_R2C
};


class WrapCUDA
{
public:
    WrapCUDA();
    virtual ~WrapCUDA();

    // CUDA functions.
    int cuInit(unsigned int flags);
    int cuCtxSetCurrent(void* context);
    int cuDeviceGet(int* device_handle, int ordinal);
    int cuDeviceGetAttribute(int* out, int attrib, int device_handle);
    int cuDeviceGetCount(int* count);
    int cuDevicePrimaryCtxRetain(void** context, int device_handle);
    int cuDevicePrimaryCtxRelease(int device_handle);
    int cuGetErrorString(int error_code, const char** str);
    int cuModuleLoadData(void** module, const void* image);
    int cuModuleUnload(void* module);
    int cuModuleGetFunction(void** func, void* module, const char* name);
    int cuMemAlloc(void** device_ptr, size_t bytes);
    int cuMemFree(void* device_ptr);
    int cuMemcpyDtoD(void* device_dst, const void* device_src, size_t bytes);
    int cuMemcpyDtoH(void* host_ptr, const void* device_ptr, size_t bytes);
    int cuMemcpyHtoD(void* device_ptr, const void* host_ptr, size_t bytes);
    int cuMemset(void* device_ptr, unsigned char value, size_t bytes);
    int cuLaunchKernel(
            void* func,
            unsigned int grid_dim_x,
            unsigned int grid_dim_y,
            unsigned int grid_dim_z,
            unsigned int block_dim_x,
            unsigned int block_dim_y,
            unsigned int block_dim_z,
            unsigned int shared_mem_bytes,
            void* stream,
            void** kernel_params,
            void** extra);

    // CUFFT functions.
    int cufftPlanMany(int* handle, int rank, const int* n, const int* inembed,
        int istride, int idist, const int* onembed, int ostride,
        int odist, int type, int batch);
    int cufftExecC2C(int handle, void* input, void* output, int dir);
    int cufftExecZ2Z(int handle, void* input, void* output, int dir);
    int cufftExecR2C(int handle, void* input, void* output);
    int cufftExecD2Z(int handle, void* input, void* output);
    int cufftDestroy(int handle);

    // NVRTC functions.
    int nvrtcAddNameExpression(void* program, const char* name_expression);
    int nvrtcCompileProgram(void* program,
            int num_options, const char* const* options);
    int nvrtcCreateProgram(void** program, const char* src,
            const char* name, int num_headers, const char* const* headers,
            const char* const* include_names);
    int nvrtcDestroyProgram(void** program);
    const char* nvrtcGetErrorString(int error_code);
    int nvrtcGetLoweredName(void* program, const char* name_expression,
            const char** lowered_name);
    int nvrtcGetProgramLogSize(void* program, size_t* size);
    int nvrtcGetProgramLog(void* program, char* log);
    int nvrtcGetPTXSize(void* program, size_t* size);
    int nvrtcGetPTX(void* program, char* ptx);

    // Utility functions.
    FFT* fft_create(int precision, int location,
        int rank, const int* n, int type, int batch, int* status);
    void fft_exec(FFT* fft, Mem* in, Mem* out, int forward, int* status);
    void fft_free(FFT* fft, int* status);
    void launch_kernel(
            const char* name,
            const size_t num_blocks[3], const size_t num_threads[3],
            size_t shared_mem_bytes, void* stream, const void** args, int* status);
    Mem* mem_create(int type, int location, size_t num_elements, int* status);
    Mem* mem_create_copy(const Mem* src, int location, int* status);
    void mem_clear_contents(Mem* mem, int* status);
    void mem_copy_contents(Mem* dst, const Mem* src,
            size_t offset_dst, size_t offset_src, size_t num_elements, int* status);
    void mem_free(Mem* mem, int* status);
    void mem_read_element(const Mem* mem, size_t index, void* out, int* status);

    // State variables.
    int init_cuda_;
    int device_;
    void* context_;
    void* module_;
    void* prog_;
    char* ptx_;

    // Kernels.
    std::map<std::string, void*> kernels_;

private:
    // CUDA function handles.
    int (*cuInit_)(unsigned int flags);
    int (*cuCtxSetCurrent_)(void* context);
    int (*cuDeviceGet_)(int* device_handle, int ordinal);
    int (*cuDeviceGetAttribute_)(int* out, int attrib, int device_handle);
    int (*cuDeviceGetCount_)(int* count);
    int (*cuDevicePrimaryCtxRetain_)(void** context, int device_handle);
    int (*cuDevicePrimaryCtxRelease_)(int device_handle);
    int (*cuGetErrorString_)(int error_code, const char** str);
    int (*cuModuleLoadData_)(void** module, const void* image);
    int (*cuModuleUnload_)(void* module);
    int (*cuModuleGetFunction_)(void** func, void* module, const char* name);
    int (*cuMemAlloc_)(void** device_ptr, size_t bytes);
    int (*cuMemFree_)(void* device_ptr);
    int (*cuMemcpyDtoD_)(void* host_ptr, const void* device_ptr, size_t bytes);
    int (*cuMemcpyDtoH_)(void* host_ptr, const void* device_ptr, size_t bytes);
    int (*cuMemcpyHtoD_)(void* device_ptr, const void* host_ptr, size_t bytes);
    int (*cuMemset_)(void* device_ptr, unsigned char value, size_t bytes);
    int (*cuLaunchKernel_)(
            void* func,
            unsigned int grid_dim_x,
            unsigned int grid_dim_y,
            unsigned int grid_dim_z,
            unsigned int block_dim_x,
            unsigned int block_dim_y,
            unsigned int block_dim_z,
            unsigned int shared_mem_bytes,
            void* stream,
            void** kernel_params,
            void** extra);

    // CUFFT function handles.
    int (*cufftPlanMany_)(int* handle, int rank, const int *n,
        const int *inembed, int istride, int idist,
        const int *onembed, int ostride, int odist, int type, int batch);
    int (*cufftExecC2C_)(int handle, void* input, void* output, int dir);
    int (*cufftExecZ2Z_)(int handle, void* input, void* output, int dir);
    int (*cufftExecR2C_)(int handle, void* input, void* output);
    int (*cufftExecD2Z_)(int handle, void* input, void* output);
    int (*cufftDestroy_)(int handle);

    // NVRTC function handles.
    int (*nvrtcAddNameExpression_)(void* program, const char* name_expression);
    int (*nvrtcCreateProgram_)(void** program, const char* src,
            const char* name, int num_headers, const char* const* headers,
            const char* const* include_names);
    int (*nvrtcCompileProgram_)(void* program,
            int num_options, const char* const* options);
    int (*nvrtcDestroyProgram_)(void** program);
    const char* (*nvrtcGetErrorString_)(int error_code);
    int (*nvrtcGetLoweredName_)(void* program, const char* name_expression,
            const char** lowered_name);
    int (*nvrtcGetProgramLogSize_)(void* program, size_t* size);
    int (*nvrtcGetProgramLog_)(void* program, char* log);
    int (*nvrtcGetPTXSize_)(void* program, size_t* size);
    int (*nvrtcGetPTX_)(void* program, char* ptx);

    // Library handles.
    void* lib_cuda_;
    void* lib_nvrtc_;
    void* lib_cufft_;

    // Private methods.
    int check_init();
    void get_cuda_functions();
    void get_cufft_functions();
    void get_nvrtc_functions();
};

// Stateless utility functions.
Mem* mem_create_alias_from_raw(void* ptr, int type, int location,
            size_t num_elements, int* status);
void* mem_buffer(Mem* mem);
const void* mem_buffer_const(const Mem* mem);
size_t mem_element_size(int type);
int mem_location(const Mem* mem);
size_t mem_num_elements(const Mem* mem);
void* mem_ptr(Mem* mem);
const void* mem_ptr_const(const Mem* mem);
int mem_type(const Mem* mem);

#ifdef __cplusplus
}
#endif

#endif /* include guard */
