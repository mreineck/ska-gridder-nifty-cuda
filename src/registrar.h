/* See the LICENSE file at the top-level directory of this distribution. */

#ifndef REGISTRAR_H_
#define REGISTRAR_H_

#include <vector>

struct SourceRegistrar
{
    static std::vector<const char*>& sources()
    {
        static std::vector<const char*> k;
        return k;
    }
    SourceRegistrar(const char* src) { sources().push_back(src); }
};

struct NameRegistrar
{
    static std::vector<const char*>& names()
    {
        static std::vector<const char*> k;
        return k;
    }
    NameRegistrar(const char* name) { names().push_back(name); }
};

#define M_CAT(A, B) M_CAT_(A, B)
#define M_CAT_(A, B) A##B

#define REGISTER_SRC(src) static SourceRegistrar M_CAT(r_, __LINE__)(src);
#define REGISTER_NAME(name) static NameRegistrar M_CAT(r_, __LINE__)(name);

#endif /* include guard */
