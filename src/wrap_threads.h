/* See the LICENSE file at the top-level directory of this distribution. */

#ifdef __cplusplus
extern "C" {
#endif

struct Mutex;
typedef struct Mutex Mutex;

/**
 * @brief Creates a mutex.
 *
 * @details
 * Creates a mutex.
 *
 * The mutex is created in an unlocked state.
 */
Mutex* mutex_create(void);

/**
 * @brief Destroys the mutex.
 *
 * @details
 * Destroys the mutex.
 *
 * @param[in,out] mutex Pointer to mutex.
 */
void mutex_free(Mutex* mutex);

/**
 * @brief Locks the mutex.
 *
 * @details
 * Locks the mutex.
 *
 * @param[in,out] mutex Pointer to mutex.
 */
void mutex_lock(Mutex* mutex);

/**
 * @brief Unlocks the mutex.
 *
 * @details
 * Unlocks the mutex.
 *
 * @param[in,out] mutex Pointer to mutex.
 */
void mutex_unlock(Mutex* mutex);

#ifdef __cplusplus
}
#endif
