/* See the LICENSE file at the top-level directory of this distribution. */

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <map>
#include <string>
#include <vector>

#include "registrar.h"
#include "wrap_gpu.h"
#include "wrap_lib.h"
#include "wrap_threads.h"

enum CUFFT_CONSTANTS
{
    CUFFT_R2C = 0x2a,
    CUFFT_C2R = 0x2c,
    CUFFT_C2C = 0x29,
    CUFFT_D2Z = 0x6a,
    CUFFT_Z2D = 0x6c,
    CUFFT_Z2Z = 0x69,
    CUFFT_FORWARD = -1,
    CUFFT_INVERSE = 1,
};

struct MyMutex
{
    Mutex* m;
    MyMutex()  { this->m = mutex_create(); }
    ~MyMutex() { mutex_free(this->m); }
    void lock()   { mutex_lock(this->m); }
    void unlock() { mutex_unlock(this->m); }
};
struct MyMutexLocker
{
    MyMutex* m_;
    MyMutexLocker(MyMutex* m) : m_(m) { m_->lock(); }
    ~MyMutexLocker() { m_->unlock(); }
};
static MyMutex mutex_;

WrapCUDA::WrapCUDA()
: init_cuda_(0)
, device_(0)
, context_(0)
, module_(0)
, ptx_(0)
, lib_cuda_(0)
, lib_cufft_(0)
, lib_nvrtc_(0)
{
    // Load extensions
    get_cuda_functions();
    get_cufft_functions();
    get_nvrtc_functions();

    // Initialize
    const int error = check_init();
    if (error) fprintf(stderr, "cuda error: %i", error);
}

WrapCUDA::~WrapCUDA()
{
    if(init_cuda_)
    {
        // Deinitialize
        if (module_) cuModuleUnload(module_);
        if (prog_) nvrtcDestroyProgram(&prog_);
        free(ptx_);
        cuDevicePrimaryCtxRelease(device_);

        // Unload Extensions
        lib_close(lib_nvrtc_);
        lib_close(lib_cufft_);
        lib_close(lib_cuda_);
    }
}


// CUDA functions.
int WrapCUDA::cuInit(unsigned int flags)
{
    if (!lib_cuda_ || !cuInit_) return ERR_GPU_NO_CUDA;
    return cuInit_(flags);
}

int WrapCUDA::cuCtxSetCurrent(void* context)
{
    if (!lib_cuda_ || !cuCtxSetCurrent_) return ERR_GPU_NO_CUDA;
    return cuCtxSetCurrent_(context);
}

int WrapCUDA::cuDeviceGet(int* device_handle, int ordinal)
{
    if (!lib_cuda_ || !cuDeviceGet_) return ERR_GPU_NO_CUDA;
    return cuDeviceGet_(device_handle, ordinal);
}

int WrapCUDA::cuDeviceGetAttribute(int* out, int attrib, int device_handle)
{
    if (!lib_cuda_ || !cuDeviceGetAttribute_) return ERR_GPU_NO_CUDA;
    return cuDeviceGetAttribute_(out, attrib, device_handle);
}

int WrapCUDA::cuDeviceGetCount(int* count)
{
    if (!lib_cuda_ || !cuDeviceGetCount_) return ERR_GPU_NO_CUDA;
    return cuDeviceGetCount_(count);
}

int WrapCUDA::cuDevicePrimaryCtxRetain(void** context, int device_handle)
{
    if (!lib_cuda_ || !cuDevicePrimaryCtxRetain_) return ERR_GPU_NO_CUDA;
    return cuDevicePrimaryCtxRetain_(context, device_handle);
}

int WrapCUDA::cuDevicePrimaryCtxRelease(int device_handle)
{
    if (!lib_cuda_ || !cuDevicePrimaryCtxRelease_) return ERR_GPU_NO_CUDA;
    return cuDevicePrimaryCtxRelease_(device_handle);
}

int WrapCUDA::cuGetErrorString(int error_code, const char** str)
{
    if (!lib_cuda_ || !cuGetErrorString_)
    {
        *str = "(CUDA not available)";
        return ERR_GPU_NO_CUDA;
    }
    return cuGetErrorString_(error_code, str);
}

int WrapCUDA::cuModuleLoadData(void** module, const void* image)
{
    if (!lib_cuda_ || !cuModuleLoadData_) return ERR_GPU_NO_CUDA;
    return cuModuleLoadData_(module, image);
}

int WrapCUDA::cuModuleUnload(void* module)
{
    if (!lib_cuda_ || !cuModuleUnload_) return ERR_GPU_NO_CUDA;
    return cuModuleUnload_(module);
}

int WrapCUDA::cuModuleGetFunction(void** func, void* module, const char* name)
{
    if (!lib_cuda_ || !cuModuleGetFunction_) return ERR_GPU_NO_CUDA;
    return cuModuleGetFunction_(func, module, name);
}

int WrapCUDA::cuMemAlloc(void** device_ptr, size_t bytes)
{
    if (!lib_cuda_ || !cuMemAlloc_) return ERR_GPU_NO_CUDA;
    const int error = check_init();
    if (error) return error;
    return cuMemAlloc_(device_ptr, bytes);
}

int WrapCUDA::cuMemFree(void* device_ptr)
{
    if (!lib_cuda_ || !cuMemFree_) return ERR_GPU_NO_CUDA;
    return cuMemFree_(device_ptr);
}

int WrapCUDA::cuMemcpyDtoD(void* device_dst, const void* device_src, size_t bytes)
{
    if (!lib_cuda_ || !cuMemcpyDtoD_) return ERR_GPU_NO_CUDA;
    return cuMemcpyDtoD_(device_dst, device_src, bytes);
}

int WrapCUDA::cuMemcpyDtoH(void* host_ptr, const void* device_ptr, size_t bytes)
{
    if (!lib_cuda_ || !cuMemcpyDtoH_) return ERR_GPU_NO_CUDA;
    return cuMemcpyDtoH_(host_ptr, device_ptr, bytes);
}

int WrapCUDA::cuMemcpyHtoD(void* device_ptr, const void* host_ptr, size_t bytes)
{
    if (!lib_cuda_ || !cuMemcpyHtoD_) return ERR_GPU_NO_CUDA;
    return cuMemcpyHtoD_(device_ptr, host_ptr, bytes);
}

int WrapCUDA::cuMemset(void* device_ptr, unsigned char value, size_t bytes)
{
    if (!lib_cuda_ || !cuMemset_) return ERR_GPU_NO_CUDA;
    return cuMemset_(device_ptr, value, bytes);
}

int WrapCUDA::cuLaunchKernel(
        void* func,
        unsigned int grid_dim_x,
        unsigned int grid_dim_y,
        unsigned int grid_dim_z,
        unsigned int block_dim_x,
        unsigned int block_dim_y,
        unsigned int block_dim_z,
        unsigned int shared_mem_bytes,
        void* stream,
        void** kernel_params,
        void** extra)
{
    if (!lib_cuda_ || !cuLaunchKernel_) return ERR_GPU_NO_CUDA;
    return cuLaunchKernel_(func, grid_dim_x, grid_dim_y, grid_dim_z,
            block_dim_x, block_dim_y, block_dim_z,
            shared_mem_bytes, stream, kernel_params, extra);
}


// CUFFT functions.
int WrapCUDA::cufftPlanMany(int* handle, int rank, const int* n,
        const int* inembed, int istride, int idist,
        const int* onembed, int ostride, int odist, int type, int batch)
{
    if (!lib_cufft_ || !cufftPlanMany_) return ERR_GPU_NO_CUFFT;
    const int error = check_init();
    if (error) return error;
    return cufftPlanMany_(handle, rank, n, inembed, istride, idist,
            onembed, ostride, odist, type, batch);
}

int WrapCUDA::cufftExecC2C(int handle, void* input, void* output, int dir)
{
    if (!lib_cufft_ || !cufftExecC2C_) return ERR_GPU_NO_CUFFT;
    return cufftExecC2C_(handle, input, output, dir);
}

int WrapCUDA::cufftExecZ2Z(int handle, void* input, void* output, int dir)
{
    if (!lib_cufft_ || !cufftExecZ2Z_) return ERR_GPU_NO_CUFFT;
    return cufftExecZ2Z_(handle, input, output, dir);
}

int WrapCUDA::cufftExecR2C(int handle, void* input, void* output)
{
    if (!lib_cufft_ || !cufftExecR2C_) return ERR_GPU_NO_CUFFT;
    return cufftExecR2C_(handle, input, output);
}

int WrapCUDA::cufftExecD2Z(int handle, void* input, void* output)
{
    if (!lib_cufft_ || !cufftExecD2Z_) return ERR_GPU_NO_CUFFT;
    return cufftExecD2Z_(handle, input, output);
}

int WrapCUDA::cufftDestroy(int handle)
{
    if (!lib_cufft_ || !cufftDestroy_) return ERR_GPU_NO_CUFFT;
    return cufftDestroy_(handle);
}


// NVRTC functions.
int WrapCUDA::nvrtcAddNameExpression(void* program, const char* name_expression)
{
    if (!lib_nvrtc_ || !nvrtcAddNameExpression_) return ERR_GPU_NO_NVRTC;
    return nvrtcAddNameExpression_(program, name_expression);
}

int WrapCUDA::nvrtcCompileProgram(void* program,
        int num_options, const char* const* options)
{
    if (!lib_nvrtc_ || !nvrtcCompileProgram_) return ERR_GPU_NO_NVRTC;
    return nvrtcCompileProgram_(program, num_options, options);
}

int WrapCUDA::nvrtcCreateProgram(void** program, const char* src,
        const char* name, int num_headers, const char* const* headers,
        const char* const* include_names)
{
    if (!lib_nvrtc_ || !nvrtcCreateProgram_) return ERR_GPU_NO_NVRTC;
    return nvrtcCreateProgram_(program, src, name,
            num_headers, headers, include_names);
}

int WrapCUDA::nvrtcDestroyProgram(void** program)
{
    if (!lib_nvrtc_ || !nvrtcDestroyProgram_) return ERR_GPU_NO_NVRTC;
    return nvrtcDestroyProgram_(program);
}

const char* WrapCUDA::nvrtcGetErrorString(int error_code)
{
    if (!lib_nvrtc_ || !nvrtcGetErrorString_) return "(NVRTC not available)";
    return nvrtcGetErrorString_(error_code);
}

int WrapCUDA::nvrtcGetLoweredName(void* program, const char* name_expression,
        const char** lowered_name)
{
    if (!lib_nvrtc_ || !nvrtcGetLoweredName_) return ERR_GPU_NO_NVRTC;
    return nvrtcGetLoweredName_(program, name_expression, lowered_name);
}

int WrapCUDA::nvrtcGetProgramLogSize(void* program, size_t* size)
{
    if (!lib_nvrtc_ || !nvrtcGetProgramLogSize_) return ERR_GPU_NO_NVRTC;
    return nvrtcGetProgramLogSize_(program, size);
}

int WrapCUDA::nvrtcGetProgramLog(void* program, char* log)
{
    if (!lib_nvrtc_ || !nvrtcGetProgramLog_) return ERR_GPU_NO_NVRTC;
    return nvrtcGetProgramLog_(program, log);
}

int WrapCUDA::nvrtcGetPTXSize(void* program, size_t* size)
{
    if (!lib_nvrtc_ || !nvrtcGetPTXSize_) return ERR_GPU_NO_NVRTC;
    return nvrtcGetPTXSize_(program, size);
}

int WrapCUDA::nvrtcGetPTX(void* program, char* ptx)
{
    if (!lib_nvrtc_ || !nvrtcGetPTX_) return ERR_GPU_NO_NVRTC;
    return nvrtcGetPTX_(program, ptx);
}


int WrapCUDA::check_init()
{
    int error = 0;
    const char* str = 0;
    MyMutexLocker locker(&mutex_);
    if (init_cuda_) return 0;

    int device_count = 0;
    int compute_major = 0, compute_minor = 0;
    error = this->cuInit(0);
    if (error)
    {
        this->cuGetErrorString(error, &str);
        fprintf(stderr, "cuInit error (code %d): %s\n", error, str);
        return error;
    }
    error = this->cuDeviceGetCount(&device_count);
    if (error || device_count == 0)
    {
        this->cuGetErrorString(error, &str);
        fprintf(stderr, "No CUDA devices found (code %d): %s\n", error, str);
        return error;
    }
    this->cuDeviceGet(&device_, 0);

    // Get device compute capability.
    char arch[32];
    this->cuDeviceGetAttribute(&compute_major, 75, device_);
    this->cuDeviceGetAttribute(&compute_minor, 76, device_);
    sprintf(arch, "-arch=compute_%d%d", compute_major, compute_minor);
    //printf("Compiling for '%s'\n", arch);

    // Get program sources.
    std::vector<const char*>& sv = SourceRegistrar::sources();
    std::string source;
    for (std::vector<const char*>::iterator i = sv.begin(); i != sv.end(); ++i)
        source.append(*i);

    // Create a program.
    error = this->nvrtcCreateProgram(&prog_, source.c_str(), "", 0, NULL, NULL);
    if (error)
    {
        fprintf(stderr, "nvrtcCreateProgram error (code %d): %s\n",
                error, this->nvrtcGetErrorString(error));
        return error;
    }

    // Add templated kernel names.
    std::vector<const char*>& nv = NameRegistrar::names();
    for (std::vector<const char*>::iterator i = nv.begin(); i != nv.end(); ++i)
    {
        error = this->nvrtcAddNameExpression(prog_, *i);
        if (error)
        {
            fprintf(stderr, "nvrtcAddNameExpression error (code %d): %s\n",
                    error, this->nvrtcGetErrorString(error));
            return error;
        }
    }

    // Compile the program for the GPU architecture.
    const char* opts[] = { arch };
    const int compile_result = this->nvrtcCompileProgram(prog_, 1, opts);

    // Check for compilation errors.
    if (compile_result != 0)
    {
        size_t log_size;
        fprintf(stderr, "nvrtcCompileProgram error (code %d): %s\n",
                compile_result, this->nvrtcGetErrorString(compile_result));
        this->nvrtcGetProgramLogSize(prog_, &log_size);
        char *log = (char*) calloc(1, log_size);
        this->nvrtcGetProgramLog(prog_, log);
        printf("%s\n", log);
        free(log);
        this->nvrtcDestroyProgram(&prog_);
        prog_ = 0;
        return compile_result;
    }

    // Get PTX from compiled program and create a module from it.
    size_t ptx_size;
    error = this->nvrtcGetPTXSize(prog_, &ptx_size);
    if (error)
    {
        fprintf(stderr, "nvrtcGetPTXSize error (code %d): %s\n",
                error, this->nvrtcGetErrorString(error));
        return error;
    }
    ptx_ = (char*) calloc(1, ptx_size);
    error = this->nvrtcGetPTX(prog_, ptx_);
    if (error)
    {
        fprintf(stderr, "nvrtcGetPTX error (code %d): %s\n",
                error, this->nvrtcGetErrorString(error));
        return error;
    }

    // Set up the device context.
    error = this->cuDevicePrimaryCtxRetain(&context_, device_);
    if (error)
    {
        this->cuGetErrorString(error, &str);
        fprintf(stderr, "cuDevicePrimaryCtxRetain error (code %d): %s\n", error, str);
        return error;
    }
    error = this->cuCtxSetCurrent(context_);
    if (error)
    {
        this->cuGetErrorString(error, &str);
        fprintf(stderr, "cuCtxSetCurrent error (code %d): %s\n", error, str);
        return error;
    }
    error = this->cuModuleLoadData(&module_, ptx_);
    if (error)
    {
        this->cuGetErrorString(error, &str);
        fprintf(stderr, "cuModuleLoadData error (code %d): %s\n", error, str);
        return error;
    }

    // Set "initialised" flag.
    init_cuda_ = 1;
    return 0;
}

void WrapCUDA::get_cuda_functions()
{
    lib_cuda_ = lib_open("libcuda");
    if (!lib_cuda_)
    {
        fprintf(stderr,
                "Unable to open 'libcuda'. Check LD_LIBRARY_PATH.\n");
        return;
    }
    *(void**) (&cuInit_) = lib_symbol(lib_cuda_, "cuInit");
    *(void**) (&cuCtxSetCurrent_) = lib_symbol(lib_cuda_, "cuCtxSetCurrent");
    *(void**) (&cuDeviceGet_) = lib_symbol(lib_cuda_, "cuDeviceGet");
    *(void**) (&cuDeviceGetAttribute_) = lib_symbol(lib_cuda_, "cuDeviceGetAttribute");
    *(void**) (&cuDeviceGetCount_) = lib_symbol(lib_cuda_, "cuDeviceGetCount");
    *(void**) (&cuDevicePrimaryCtxRetain_) = lib_symbol(lib_cuda_, "cuDevicePrimaryCtxRetain");
    *(void**) (&cuDevicePrimaryCtxRelease_) = lib_symbol(lib_cuda_, "cuDevicePrimaryCtxRelease");
    *(void**) (&cuGetErrorString_) = lib_symbol(lib_cuda_, "cuGetErrorString");
    *(void**) (&cuModuleLoadData_) = lib_symbol(lib_cuda_, "cuModuleLoadData");
    *(void**) (&cuModuleUnload_) = lib_symbol(lib_cuda_, "cuModuleUnload");
    *(void**) (&cuModuleGetFunction_) = lib_symbol(lib_cuda_, "cuModuleGetFunction");
    *(void**) (&cuMemAlloc_) = lib_symbol(lib_cuda_, "cuMemAlloc_v2");
    *(void**) (&cuMemFree_) = lib_symbol(lib_cuda_, "cuMemFree_v2");
    *(void**) (&cuMemcpyDtoD_) = lib_symbol(lib_cuda_, "cuMemcpyDtoD_v2");
    *(void**) (&cuMemcpyDtoH_) = lib_symbol(lib_cuda_, "cuMemcpyDtoH_v2");
    *(void**) (&cuMemcpyHtoD_) = lib_symbol(lib_cuda_, "cuMemcpyHtoD_v2");
    *(void**) (&cuMemset_) = lib_symbol(lib_cuda_, "cuMemsetD8_v2");
    *(void**) (&cuLaunchKernel_) = lib_symbol(lib_cuda_, "cuLaunchKernel");
}

void WrapCUDA::get_cufft_functions()
{
    lib_cufft_ = lib_open("libcufft");
    if (!lib_cufft_)
    {
        fprintf(stderr,
                "Unable to open 'libcufft'. Check LD_LIBRARY_PATH.\n");
        return;
    }
    *(void**) (&cufftPlanMany_) = lib_symbol(lib_cufft_, "cufftPlanMany");
    *(void**) (&cufftExecC2C_) = lib_symbol(lib_cufft_, "cufftExecC2C");
    *(void**) (&cufftExecZ2Z_) = lib_symbol(lib_cufft_, "cufftExecZ2Z");
    *(void**) (&cufftExecR2C_) = lib_symbol(lib_cufft_, "cufftExecR2C");
    *(void**) (&cufftExecD2Z_) = lib_symbol(lib_cufft_, "cufftExecD2Z");
    *(void**) (&cufftDestroy_) = lib_symbol(lib_cufft_, "cufftDestroy");
}

void WrapCUDA::get_nvrtc_functions()
{
    lib_nvrtc_ = lib_open("libnvrtc");
    if (!lib_nvrtc_)
    {
        fprintf(stderr,
                "Unable to open 'libnvrtc'. Check LD_LIBRARY_PATH.\n");
        return;
    }
    *(void**) (&nvrtcAddNameExpression_) = lib_symbol(lib_nvrtc_, "nvrtcAddNameExpression");
    *(void**) (&nvrtcCreateProgram_) = lib_symbol(lib_nvrtc_, "nvrtcCreateProgram");
    *(void**) (&nvrtcCompileProgram_) = lib_symbol(lib_nvrtc_, "nvrtcCompileProgram");
    *(void**) (&nvrtcDestroyProgram_) = lib_symbol(lib_nvrtc_, "nvrtcDestroyProgram");
    *(void**) (&nvrtcGetErrorString_) = lib_symbol(lib_nvrtc_, "nvrtcGetErrorString");
    *(void**) (&nvrtcGetLoweredName_) = lib_symbol(lib_nvrtc_, "nvrtcGetLoweredName");
    *(void**) (&nvrtcGetProgramLogSize_) = lib_symbol(lib_nvrtc_, "nvrtcGetProgramLogSize");
    *(void**) (&nvrtcGetProgramLog_) = lib_symbol(lib_nvrtc_, "nvrtcGetProgramLog");
    *(void**) (&nvrtcGetPTXSize_) = lib_symbol(lib_nvrtc_, "nvrtcGetPTXSize");
    *(void**) (&nvrtcGetPTX_) = lib_symbol(lib_nvrtc_, "nvrtcGetPTX");
}


void WrapCUDA::launch_kernel(
        const char* name,
        const size_t num_blocks[3], const size_t num_threads[3],
        size_t shared_mem_bytes, void* stream, const void** args, int* status)
{
    int error = 0;
    if (*status) return;

    // Check the GPU wrapper initialised successfully.
    if (!init_cuda_)
    {
        *status = ERR_GPU_NO_CUDA;
        return;
    }

    // See if the kernel is cached.
    void* kernel = kernels_[std::string(name)];
    if (!kernel)
    {
        // If kernel is not cached, get it from the module.
        // Get "lowered" (mangled) name, if applicable.
        // If it isn't found, just use the original.
        const char* lowered_name = 0;
        error = nvrtcGetLoweredName(prog_, name, &lowered_name);
        if (error)
            lowered_name = name;

        // Look up the kernel.
        error = cuModuleGetFunction(&kernel,
                module_, lowered_name);
        if (error)
        {
            const char* str = 0;
            cuGetErrorString(error, &str);
            fprintf(stderr,
                    "cuModuleGetFunction error (code %d) for kernel %s: %s\n",
                    error, name, str);
            *status = ERR_GPU_KERNEL_NOT_FOUND;
            return;
        }

        // Update the cache.
        kernels_[std::string(name)] = kernel;
    }
    if (!kernel)
    {
        *status = ERR_GPU_KERNEL_NOT_FOUND;
        fprintf(stderr, "Kernel '%s' not known.\n", name);
        return;
    }
    // printf("Running kernel %s...\n", name);

    // Launch the kernel.
    error = cuLaunchKernel(kernel,
            num_blocks[0], num_blocks[1], num_blocks[2],
            num_threads[0], num_threads[1], num_threads[2],
            shared_mem_bytes, stream, const_cast<void**>(args), 0);
    if (error)
    {
        const char* str = 0;
        cuGetErrorString(error, &str);
        fprintf(stderr, "cuLaunchKernel error (code %d): %s\n", error, str);
        *status = ERR_GPU_KERNEL_LAUNCH_FAILURE;
    }
}

struct Mem
{
    int type;            /* Enumerated memory element type. */
    int location;        /* Enumerated memory address space. */
    size_t num_elements; /* Number of elements allocated. */
    int owner;           /* True if memory is owned. */
    void* data;          /* Data pointer. */
};

struct FFT
{
    int plan, precision, location, fft_type;
};

FFT* WrapCUDA::fft_create(int precision, int location,
        int rank, const int* n, int type, int batch, int* status)
{
    FFT* fft = (FFT*) calloc(1, sizeof(FFT));
    fft->precision = precision;
    fft->location = location; // For future use?
    switch (precision)
    {
    case MEM_DOUBLE:
        switch (type)
        {
        case FFT_C2C:
            fft->fft_type = CUFFT_Z2Z;
            break;
        case FFT_R2C:
            fft->fft_type = CUFFT_D2Z;
            break;
        default:
            *status = ERR_MEM_BAD_TYPE;
            return fft;
        }
        break;
    case MEM_FLOAT:
        switch (type)
        {
        case FFT_C2C:
            fft->fft_type = CUFFT_C2C;
            break;
        case FFT_R2C:
            fft->fft_type = CUFFT_R2C;
            break;
        default:
            *status = ERR_MEM_BAD_TYPE;
            return fft;
        }
        break;
    default:
        *status = ERR_MEM_BAD_TYPE;
        return fft;
    }
    int total_elements = 1;
    for (int i = 0; i < rank; ++i) total_elements *= n[i];
    const int error = cufftPlanMany(&fft->plan,
            rank, n, 0, 1, total_elements, 0, 1, total_elements,
            fft->fft_type, batch);
    if (error)
    {
        fprintf(stderr, "cufftPlanMany error (code %d)\n", error);
        *status = ERR_GPU_NO_CUFFT;
    }
    return fft;
}

void WrapCUDA::fft_exec(FFT* fft, Mem* in, Mem* out, int forward, int* status)
{
    int error = 0;
    if (*status) return;
    const int fft_dir = forward ? CUFFT_FORWARD : CUFFT_INVERSE;
    switch (fft->fft_type)
    {
    case CUFFT_C2C:
        error = cufftExecC2C(fft->plan, in->data, out->data, fft_dir);
        break;
    case CUFFT_Z2Z:
        error = cufftExecZ2Z(fft->plan, in->data, out->data, fft_dir);
        break;
    case CUFFT_D2Z:
        error = cufftExecD2Z(fft->plan, in->data, out->data);
        break;
    case CUFFT_R2C:
        error = cufftExecR2C(fft->plan, in->data, out->data);
        break;
    default:
        *status = ERR_MEM_BAD_TYPE;
        break;
    }
    if (error)
    {
        fprintf(stderr, "cufftExec error (code %d)\n", error);
        *status = ERR_GPU_NO_CUFFT;
    }
}

void WrapCUDA::fft_free(FFT* fft, int* status)
{
    (void)status;
    cufftDestroy(fft->plan);
    free(fft);
}

size_t mem_element_size(int type)
{
    switch (type)
    {
    case MEM_CHAR:            return sizeof(char);
    case MEM_INT:             return sizeof(int);
    case MEM_FLOAT:           return sizeof(float);
    case MEM_DOUBLE:          return sizeof(double);
    case MEM_COMPLEX_FLOAT:   return 2*sizeof(float);
    case MEM_COMPLEX_DOUBLE:  return 2*sizeof(double);
    default:                  return 0;
    }
}

void* mem_buffer(Mem* mem)
{
    return &mem->data;
}

const void* mem_buffer_const(const Mem* mem)
{
    return &mem->data;
}

Mem* WrapCUDA::mem_create(int type, int location, size_t num_elements, int* status)
{
    int error;
    Mem* mem = (Mem*) calloc(1, sizeof(Mem));
    mem->type = type;
    mem->location = location;
    mem->owner = 1;
    if (!status || *status || num_elements == 0)
        return mem;

    // Get the element size and total amount of memory requested.
    const size_t element_size = mem_element_size(type);
    if (element_size == 0)
    {
        *status = ERR_MEM_BAD_TYPE;
        return mem;
    }
    mem->num_elements = num_elements;
    const size_t bytes = num_elements * element_size;

    // Check whether the memory should be on the host or the device.
    switch (location)
    {
    case MEM_CPU:
        mem->data = calloc(bytes, 1);
        if (!mem->data)
        {
            *status = ERR_MEM_ALLOC_FAILURE;
            return mem;
        }
        break;
    case MEM_GPU:
        error = cuMemAlloc(&mem->data, bytes);
        if (!mem->data || error)
        {
            const char* str = 0;
            cuGetErrorString(error, &str);
            fprintf(stderr, "cuMemAlloc error (code %d): %s\n", error, str);
            *status = ERR_MEM_ALLOC_FAILURE;
            if (mem->data)
            {
                cuMemFree(mem->data);
                mem->data = 0;
            }
            return mem;
        }
        break;
    default:
        *status = ERR_MEM_BAD_LOCATION;
        break;
    };
    return mem;
}

Mem* mem_create_alias_from_raw(void* ptr, int type, int location,
        size_t num_elements, int* status)
{
    Mem* mem = (Mem*) calloc(1, sizeof(Mem));
    if (!mem)
    {
        *status = ERR_MEM_ALLOC_FAILURE;
        return 0;
    }
    mem->type = type;
    mem->location = location;
    mem->num_elements = num_elements;
    mem->owner = 0;
    mem->data = ptr;
    return mem;
}

Mem* WrapCUDA::mem_create_copy(const Mem* src, int location, int* status)
{
    Mem* mem = 0;
    if (*status) return 0;
    mem = mem_create(src->type, location, src->num_elements, status);
    if (!mem || *status)
        return mem;
    mem_copy_contents(mem, src, 0, 0, src->num_elements, status);
    return mem;
}

void WrapCUDA::mem_clear_contents(Mem* mem, int* status)
{
    if (*status || mem->num_elements == 0) return;
    const size_t size = mem->num_elements * mem_element_size(mem->type);
    if (mem->location == MEM_CPU)
        memset(mem->data, 0, size);
    else if (mem->location == MEM_GPU)
        cuMemset(mem->data, 0, size);
    else
        *status = ERR_MEM_BAD_LOCATION;
}

void WrapCUDA::mem_copy_contents(Mem* dst, const Mem* src,
        size_t offset_dst, size_t offset_src, size_t num_elements, int* status)
{
    int error = 0;
    if (*status) return;
    if (src->num_elements == 0 || num_elements == 0) return;
    const size_t element_size = mem_element_size(src->type);
    const size_t bytes        = element_size * num_elements;
    const size_t start_dst    = element_size * offset_dst;
    const size_t start_src    = element_size * offset_src;
    const int location_src    = src->location;
    const int location_dst    = dst->location;
    const void *p_src = (const void*)((const char*)(src->data) + start_src);
    void* p_dst       = (void*)((char*)(dst->data) + start_dst);

	//printf("num_elements is %i\n", num_elements); // AG 
	//printf("element_size is %i\n", element_size); // AG 
	
    if (location_src == MEM_CPU && location_dst == MEM_CPU)
        memcpy(p_dst, p_src, bytes);
    else if (location_src == MEM_CPU && location_dst == MEM_GPU)
        error = (int)cuMemcpyHtoD(p_dst, p_src, bytes);
    else if (location_src == MEM_GPU && location_dst == MEM_CPU)
        error = (int)cuMemcpyDtoH(p_dst, p_src, bytes);
    else if (location_src == MEM_GPU && location_dst == MEM_GPU)
        error = (int)cuMemcpyDtoD(p_dst, p_src, bytes);
    else
        *status = ERR_MEM_BAD_LOCATION;

    if (error)
    {
        const char* str = 0;
        cuGetErrorString(error, &str);
        fprintf(stderr, "cuMemcpy error (code %d): %s\n", error, str);
        *status = ERR_MEM_COPY_FAILURE;
    }
}

void WrapCUDA::mem_free(Mem* mem, int* status)
{
    if (!mem) return;
    if (mem->owner && mem->data)
    {
        switch (mem->location)
        {
        case MEM_CPU:
            free(mem->data);
            break;
        case MEM_GPU:
            cuMemFree(mem->data);
            break;
        default:
            *status = ERR_MEM_BAD_LOCATION;
            break;
        };
    }
    free(mem);
}

int mem_location(const Mem* mem)
{
    return mem->location;
}

size_t mem_num_elements(const Mem* mem)
{
    return mem->num_elements;
}

void* mem_ptr(Mem* mem)
{
    return mem->data;
}

const void* mem_ptr_const(const Mem* mem)
{
    return mem->data;
}

void WrapCUDA::mem_read_element(const Mem* mem, size_t index, void* out, int* status)
{
    if (*status) return;
    const size_t bytes = mem_element_size(mem->type);
    const size_t offset = bytes * index;
    if (mem->location == MEM_CPU)
    {
        const char* from = ((const char*) mem->data) + offset;
        memcpy(out, (const void*)from, bytes);
    }
    else if (mem->location == MEM_GPU)
    {
        const char* from = ((const char*) mem->data) + offset;
        cuMemcpyDtoH(out, (const void*)from, bytes);
    }
    else
        *status = ERR_MEM_BAD_LOCATION;
}

int mem_type(const Mem* mem)
{
    return mem->type;
}
