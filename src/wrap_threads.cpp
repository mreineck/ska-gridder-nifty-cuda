/* See the LICENSE file at the top-level directory of this distribution. */

#include "wrap_threads.h"
#include <stdlib.h>

#if (defined(WIN32) || defined(_WIN32) || defined(__WIN32__))
#define WRAP_THREADS_OS_WIN32
#endif
#if (defined(WIN64) || defined(_WIN64) || defined(__WIN64__))
#define WRAP_THREADS_OS_WIN64
#endif

#if (defined(WRAP_THREADS_OS_WIN32) || defined(WRAP_THREADS_OS_WIN64))
#define WRAP_THREADS_OS_WIN
#endif

#ifdef WRAP_THREADS_OS_WIN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <process.h>
#else
#include <pthread.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

struct Mutex
{
#ifdef WRAP_THREADS_OS_WIN
    CRITICAL_SECTION lock;
#else
    pthread_mutex_t lock;
#endif
};

static void mutex_init(Mutex* mutex)
{
#ifdef WRAP_THREADS_OS_WIN
    InitializeCriticalSectionAndSpinCount(&mutex->lock, 0x0800);
#else
    pthread_mutex_init(&mutex->lock, NULL);
#endif
}

static void mutex_uninit(Mutex* mutex)
{
#ifdef WRAP_THREADS_OS_WIN
    DeleteCriticalSection(&mutex->lock);
#else
    pthread_mutex_destroy(&mutex->lock);
#endif
}

Mutex* mutex_create(void)
{
    Mutex* mutex;
    mutex = (Mutex*) calloc(1, sizeof(Mutex));
    mutex_init(mutex);
    return mutex;
}

void mutex_free(Mutex* mutex)
{
    if (!mutex) return;
    mutex_uninit(mutex);
    free(mutex);
}

void mutex_lock(Mutex* mutex)
{
#ifdef WRAP_THREADS_OS_WIN
    EnterCriticalSection(&mutex->lock);
#else
    pthread_mutex_lock(&mutex->lock);
#endif
}

void mutex_unlock(Mutex* mutex)
{
#ifdef WRAP_THREADS_OS_WIN
    LeaveCriticalSection(&mutex->lock);
#else
    pthread_mutex_unlock(&mutex->lock);
#endif
}

#ifdef __cplusplus
}
#endif
