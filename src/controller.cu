
// Copyright 2021 Adam Campbell, Anthony Griffin, Andrew Ensor, Seth Hall
// Copyright 2021 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "controller.h"

#define NOT_PYTHON 1
#include "nifty_kernels.cuh"

// Use of constant buffers instead of cudaMalloc'd memory allows for
// more efficient caching, and "broadcasting" across threads
__constant__ PRECISION quadrature_nodes[QUADRATURE_SUPPORT_BOUND];
__constant__ PRECISION quadrature_weights[QUADRATURE_SUPPORT_BOUND];
__constant__ PRECISION quadrature_kernel[QUADRATURE_SUPPORT_BOUND];

int execute_imaging_pipeline(config *config)
{
    host_memory_handles host;
    device_memory_handles device;
    init_memory_handles(&host, &device);

    // Allocate host based memory
    allocate_host_bound_buffers(&host, &device, config);

    // Populate synthetic visibilities
    // generate_synthetic_visibilities(&host, config);

    // Populate vis from file
    load_visibilities_from_file(&host, config);

    // Calculate gauss-legendre quadrature values for convolution correction
    generate_gauss_legendre_conv_kernel(&host, config);

    // Perform gridding and degridding stages
    perform_nifty_gridding_degridding(&host, &device, config);

    // Clean up
    clean_up_memory_handles(&host, &device);
    cudaDeviceReset();

    return EXIT_SUCCESS;
}

void init_memory_handles(host_memory_handles *host, device_memory_handles *device)
{
    host->dirty_image        = NULL;
    host->w_grid_stack       = NULL;
    host->visibilities       = NULL;
    host->vis_weights        = NULL;
    host->uvw_coords         = NULL;
    host->conv_corr_kernel   = NULL;
    host->quadrature_nodes   = NULL;
    host->quadrature_weights = NULL;
    host->quadrature_kernel  = NULL;
    
    device->d_dirty_image      = NULL;
    device->d_w_grid_stack     = NULL;
    device->d_visibilities     = NULL;
    device->d_vis_weights      = NULL;
    device->d_uvw_coords       = NULL;
    device->d_conv_corr_kernel = NULL;
    device->fft_plan           = NULL;
}

// Utilises pinned memory for faster I/O with the GPU
void allocate_host_bound_buffers(host_memory_handles *host, device_memory_handles *device, config *config)
{
    uint32_t num_dirty_image_pixels = config->image_size * config->image_size;
	
	//LogF(config, 0, "DI is %i by %i or %i total pixels.\n", config->image_size, config->image_size, num_dirty_image_pixels);
    //cudaHostAlloc(&(host->dirty_image), num_dirty_image_pixels * sizeof(PRECISION), cudaHostAllocDefault);
    CUDA_CHECK_RETURN(cudaHostAlloc(&(host->dirty_image), num_dirty_image_pixels * sizeof(PRECISION), cudaHostAllocDefault));

    uint32_t num_w_grid_stack_cells = config->grid_size * config->grid_size * config->num_w_grids_batched;
    CUDA_CHECK_RETURN(cudaHostAlloc(&(host->w_grid_stack), num_w_grid_stack_cells * sizeof(PRECISION2), cudaHostAllocDefault));
    
    uint32_t num_vis = config->num_visibilities;
    CUDA_CHECK_RETURN(cudaHostAlloc(&(host->visibilities), num_vis * sizeof(VIS_PRECISION2), cudaHostAllocDefault));
    CUDA_CHECK_RETURN(cudaHostAlloc(&(host->vis_weights), num_vis * sizeof(VIS_PRECISION), cudaHostAllocDefault));
    CUDA_CHECK_RETURN(cudaHostAlloc(&(host->uvw_coords), num_vis * sizeof(PRECISION3), cudaHostAllocDefault));

    CUDA_CHECK_RETURN(cudaHostAlloc(&(host->conv_corr_kernel), (config->image_size/2 + 1) * sizeof(PRECISION), cudaHostAllocDefault));

    host->quadrature_nodes   = (PRECISION*) calloc(QUADRATURE_SUPPORT_BOUND, sizeof(PRECISION));
    host->quadrature_weights = (PRECISION*) calloc(QUADRATURE_SUPPORT_BOUND, sizeof(PRECISION));
    host->quadrature_kernel  = (PRECISION*) calloc(QUADRATURE_SUPPORT_BOUND, sizeof(PRECISION));
}

void clean_up_memory_handles(host_memory_handles *host, device_memory_handles *device)
{
    // Freeing up CUDA host allocated memory (ie: pinned for efficient IO to/from device)
    if(host->dirty_image)      CUDA_CHECK_RETURN(cudaFreeHost(host->dirty_image));      host->dirty_image      = NULL;
    if(host->w_grid_stack)     CUDA_CHECK_RETURN(cudaFreeHost(host->w_grid_stack));     host->w_grid_stack     = NULL;
    if(host->visibilities)     CUDA_CHECK_RETURN(cudaFreeHost(host->visibilities));     host->visibilities     = NULL;
    if(host->vis_weights)      CUDA_CHECK_RETURN(cudaFreeHost(host->vis_weights));      host->vis_weights      = NULL;
    if(host->uvw_coords)       CUDA_CHECK_RETURN(cudaFreeHost(host->uvw_coords));       host->uvw_coords       = NULL;
    if(host->conv_corr_kernel) CUDA_CHECK_RETURN(cudaFreeHost(host->conv_corr_kernel)); host->conv_corr_kernel = NULL;

    // Not pinned, just standard local dynamically allocated memory
    if(host->quadrature_nodes)   free(host->quadrature_nodes);   host->quadrature_nodes   = NULL;
    if(host->quadrature_weights) free(host->quadrature_weights); host->quadrature_weights = NULL;
    if(host->quadrature_kernel)  free(host->quadrature_kernel);  host->quadrature_kernel  = NULL;

    if(device->d_dirty_image) 
        CUDA_CHECK_RETURN(cudaFree(device->d_dirty_image));
    device->d_dirty_image = NULL;

    if(device->d_w_grid_stack) 
        CUDA_CHECK_RETURN(cudaFree(device->d_w_grid_stack));
    device->d_w_grid_stack = NULL;

    if(device->d_visibilities) 
        CUDA_CHECK_RETURN(cudaFree(device->d_visibilities));
    device->d_visibilities = NULL;

    if(device->d_vis_weights) 
        CUDA_CHECK_RETURN(cudaFree(device->d_vis_weights));
    device->d_vis_weights = NULL;

    if(device->d_uvw_coords) 
        CUDA_CHECK_RETURN(cudaFree(device->d_uvw_coords));
    device->d_uvw_coords = NULL;

    if(device->d_conv_corr_kernel) 
        CUDA_CHECK_RETURN(cudaFree(device->d_conv_corr_kernel));
    device->d_conv_corr_kernel = NULL;

    if(device->fft_plan)
        CUFFT_SAFE_CALL(cufftDestroy(*(device->fft_plan)));
    device->fft_plan = NULL;
}

void perform_nifty_gridding_degridding(host_memory_handles *host, device_memory_handles *device, config *config)
{
    // Perform set up (ie: allocate memory, copy over data, etc.)
    nifty_init(host, device, config);

    // Allow CUDA to determine block/thread distribution
    // following https://developer.nvidia.com/blog/cuda-pro-tip-occupancy-api-simplifies-launch-configuration/
    int min_cuda_grid_size;
    int cuda_block_size;
    cudaOccupancyMaxPotentialBlockSize(&min_cuda_grid_size, &cuda_block_size, nifty_gridding<VIS_PRECISION, VIS_PRECISION2, PRECISION, PRECISION2, PRECISION3>, 0, 0);
    int cuda_grid_size = (config->num_visibilities + cuda_block_size - 1) / cuda_block_size;  // create 1 thread per visibility to be gridded
    LogF(config, 1, "Gridder using grid size %i, where minimum grid size available is %i, and using block size %i.\n",
		cuda_grid_size, min_cuda_grid_size, cuda_block_size);
    LogF(config, 1, "Gridder calling nifty_gridding kernel.\n");

    // Determine the block/thread distribution per w correction and summation batch
    // note: based on image size, not grid size, as we take inner region and discard padding
    dim3 w_correction_threads(
        min((uint32_t)32, (config->image_size + 1) / 2),
        min((uint32_t)32, (config->image_size + 1) / 2)
    );
    dim3 w_correction_blocks(
        (config->image_size/2 + 1 + w_correction_threads.x - 1) / w_correction_threads.x,  // allow extra in negative x quadrants, for asymmetric image centre
        (config->image_size/2 + 1 + w_correction_threads.y - 1) / w_correction_threads.y   // allow extra in negative y quadrants, for asymmetric image centre
    );

    // Determine how many w grid subset batches to process in total
    uint32_t total_w_grid_batches = (config->num_total_w_grids + config->num_w_grids_batched - 1) / config->num_w_grids_batched;
    uint32_t num_w_grid_stack_cells = config->grid_size * config->grid_size * config->num_w_grids_batched;

    dim3 scalar_threads(
        min((uint32_t)32, config->grid_size),
        min((uint32_t)32, config->grid_size)
    );
    dim3 scalar_blocks(
        (int)ceil((double)config->grid_size / scalar_threads.x), 
        (int)ceil((double)config->grid_size / scalar_threads.y)
    );

    // // Simulate major cycles (ie: perform gridding on predicted visibilities)
    for(int cycle = 0; cycle < 1; cycle++)
    {
        uint32_t num_dirty_image_pixels = config->image_size * config->image_size;
        CUDA_CHECK_RETURN(cudaMemset(device->d_dirty_image, 0, num_dirty_image_pixels * sizeof(PRECISION)));
        CUDA_CHECK_RETURN( cudaGetLastError() );
        CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
        CUDA_CHECK_RETURN(cudaMemset(device->d_w_grid_stack, 0, num_w_grid_stack_cells * sizeof(PRECISION2)));
        CUDA_CHECK_RETURN( cudaGetLastError() );
        CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

        for(int batch = 0; batch < total_w_grid_batches; batch++)
        {
            uint32_t num_w_grids_subset = min(
                config->num_w_grids_batched,
                config->num_total_w_grids - ((batch * config->num_w_grids_batched) % config->num_total_w_grids)
            );

            int32_t grid_start_w = batch*config->num_w_grids_batched;

			LogF(config, 2, "Gridder calling nifty_gridding kernel for w grids %i to %i (plane(s) in current batch).\n",
				grid_start_w, (grid_start_w + (int32_t)num_w_grids_subset - 1), num_w_grids_subset);
            
            // Perform gridding on a "chunk" of w grids
            nifty_gridding<VIS_PRECISION, VIS_PRECISION2, PRECISION, PRECISION2, PRECISION3><<<cuda_grid_size, cuda_block_size>>>(
                config->num_visibilities,
				1, // hard-coded number of channels for now!!!
                device->d_visibilities,
                device->d_vis_weights,
                device->d_uvw_coords,
                device->d_w_grid_stack,
                config->grid_size,
                grid_start_w,
                num_w_grids_subset,
                config->support,
                config->beta,
                //config->upsampling,
                config->uv_scale,
                config->w_scale, 
                config->min_plane_w,
				config->freq / PRECISION(C),
                config->generating_psf,
                config->perform_shift_fft,
                true // gridding
            );
            CUDA_CHECK_RETURN( cudaGetLastError() );
            CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

            /**************************************************************************************
             * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
             *************************************************************************************/
            // // Copy back and save each w grid to file for review of accuracy
            // std::cout << "Copying back grid stack from device to host..." << std::endl; 
            // proof_of_concept_copy_w_grid_stack_to_host(host, device, config);
            // // Save each w grid into single file of complex type, use python to render via matplotlib
            // std::cout << "Saving grid stack to file..." << std::endl;
            // proof_of_concept_save_w_grids_to_file(host, config, batch);
            /**************************************************************************************
             * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
             *************************************************************************************/

            // Perform 2D FFT on each bound w grid
            CUFFT_SAFE_CALL(CUFFT_EXECUTE_C2C(*(device->fft_plan), device->d_w_grid_stack, device->d_w_grid_stack, CUFFT_INVERSE));
            CUDA_CHECK_RETURN( cudaGetLastError() );
            CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

            // if(cycle == 0)
            // {
            //     scale_for_FFT<<<scalar_blocks, scalar_threads>>>(
            //         device->d_w_grid_stack, 
            //         num_w_grids_subset, 
            //         config->grid_size, 
            //         1.0/(config->grid_size*config->grid_size)
            //     );
            // }
            
            /**************************************************************************************
             * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
             *************************************************************************************/
            // // Copy back and save each w grid to file for review of accuracy
            // std::cout << "Copying back grid stack from device to host..." << std::endl; 
            // proof_of_concept_copy_w_grid_stack_to_host(host, device, config);
            // // Save each w grid into single file of complex type, use python to render via matplotlib
            // std::cout << "Saving grid stack to file..." << std::endl;
            // proof_of_concept_save_w_images_to_file(host, config, batch);
            /**************************************************************************************
             * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
             *************************************************************************************/
            
    		// Perform phase shift on a "chunk" of planes and sum into single real plane
            apply_w_screen_and_sum<PRECISION, PRECISION2><<<w_correction_blocks, w_correction_threads>>>(
                device->d_dirty_image,
                config->image_size,
                config->pixel_size,
                device->d_w_grid_stack,
                config->grid_size,
                grid_start_w,
                num_w_grids_subset,
                PRECISION(1.0)/config->w_scale,
                config->min_plane_w,
                config->perform_shift_fft
            );
            CUDA_CHECK_RETURN( cudaGetLastError() );
            CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

            // Reset intermediate w grid buffer for next batch
            if(batch < total_w_grid_batches - 1)
            {
                LogF(config, 3, "Resetting device w grid stack memory for next batch...\n");
                CUDA_CHECK_RETURN(cudaMemset(device->d_w_grid_stack, 0, num_w_grid_stack_cells * sizeof(PRECISION2)));
                CUDA_CHECK_RETURN( cudaGetLastError() );
                CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
            }
        }

        // Need to determine normalisation factor for scaling runtime calculated
        // conv correction values for coordinate n (where n = sqrt(1 - l^2 - m^2) - 1)
        uint32_t p = (uint32_t)(CEIL(PRECISION(1.5)*config->support + PRECISION(2.0)));
        PRECISION conv_corr_norm_factor = 0.0;
        for(uint32_t i = 0; i < p; i++)
            conv_corr_norm_factor += host->quadrature_kernel[i]*host->quadrature_weights[i];
        conv_corr_norm_factor *= (PRECISION)config->support;

        // Need to determine final scaling factor for scaling dirty image by w grid accumulation
        PRECISION inv_w_range = 1.0 / (config->max_plane_w - config->min_plane_w);

        // Need sum of visibility weights for scaling dirty image
        PRECISION vis_weights_sum = 0.0;
        for(uint32_t v = 0; v < config->num_visibilities; v++)
            vis_weights_sum += host->vis_weights[v];

        LogF(config, 3, "Sum of weights: %f\n", vis_weights_sum * config->num_channels);

        // Perform convolution correction and final scaling on single real plane
        // note: can recycle same block/thread dims as w correction kernel
        conv_corr_and_scaling<PRECISION><<<w_correction_blocks, w_correction_threads>>>(
            device->d_dirty_image,
            config->image_size,
            config->pixel_size,
            config->support,
            conv_corr_norm_factor,
            device->d_conv_corr_kernel,
            inv_w_range,
            vis_weights_sum * config->num_channels,
            PRECISION(1.0)/config->w_scale,
			quadrature_kernel,
			quadrature_nodes,
			quadrature_weights,
            true // solving
        );
        CUDA_CHECK_RETURN( cudaGetLastError() );
        CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

        // Copy back images, save to file
        copy_dirty_image_to_host(host, device, config);
        save_image_to_file(host, config, cycle);

        if(cycle == 1)
        {
            printf("EARLY EXIT FOR PREDICTION...\n");
            exit(0);
        }

        // Perform degridding here..
        CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
        CUDA_CHECK_RETURN(cudaMemset(device->d_visibilities, 0, sizeof(VIS_PRECISION2) * config->num_visibilities));
        CUDA_CHECK_RETURN( cudaGetLastError() );
        CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
        CUDA_CHECK_RETURN(cudaMemset(device->d_w_grid_stack, 0, num_w_grid_stack_cells * sizeof(PRECISION2)));
        CUDA_CHECK_RETURN( cudaGetLastError() );
        CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

        // 1. Undo convolution correction and scaling
        conv_corr_and_scaling<PRECISION><<<w_correction_blocks, w_correction_threads>>>(
            device->d_dirty_image,
            config->image_size,
            config->pixel_size,
            config->support,
            conv_corr_norm_factor,
            device->d_conv_corr_kernel,
            inv_w_range,
            vis_weights_sum * config->num_channels,
            PRECISION(1.0)/config->w_scale,
			quadrature_kernel,
			quadrature_nodes,
			quadrature_weights,
            false // predicting
        );
        CUDA_CHECK_RETURN( cudaGetLastError() );
        CUDA_CHECK_RETURN( cudaDeviceSynchronize() );


        // Copy back images, save to file
        //copy_dirty_image_to_host(host, device, config);
        //save_image2_to_file(host, config, cycle);

        for(int batch = 0; batch < total_w_grid_batches; batch++)
        {
            uint32_t num_w_grids_subset = min(
                config->num_w_grids_batched,
                config->num_total_w_grids - ((batch * config->num_w_grids_batched) % config->num_total_w_grids)
            );

            int32_t grid_start_w = batch*config->num_w_grids_batched;

            LogF(config, 3, "Reversing...\n");

            // 2. Undo w-stacking and dirty image accumulation
            reverse_w_screen_to_stack<PRECISION, PRECISION2><<<w_correction_blocks, w_correction_threads>>>(
                device->d_dirty_image,
                config->image_size,
                config->pixel_size,
                device->d_w_grid_stack,
                config->grid_size,
                grid_start_w,
                num_w_grids_subset,
                PRECISION(1.0)/config->w_scale,
                config->min_plane_w,
                config->perform_shift_fft
            );

            CUDA_CHECK_RETURN( cudaGetLastError() );
            CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

            LogF(config, 3, "Reversing FINISHED...\n");

            /**************************************************************************************
             * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
             *************************************************************************************/
            // Copy back and save each w grid to file for review of accuracy
            // std::cout << "Copying back grid stack from device to host..." << std::endl; 
            // proof_of_concept_copy_w_grid_stack_to_host(host, device, config);
            // // Save each w grid into single file of complex type, use python to render via matplotlib
            // std::cout << "Saving grid stack to file..." << std::endl;
            // proof_of_concept_save_w_images_to_file(host, config, batch);
            /**************************************************************************************
             * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
             *************************************************************************************/

            // 3. Batch FFT
            CUFFT_SAFE_CALL(CUFFT_EXECUTE_C2C(*(device->fft_plan), device->d_w_grid_stack, device->d_w_grid_stack, CUFFT_FORWARD));
            CUDA_CHECK_RETURN( cudaGetLastError() );
            CUDA_CHECK_RETURN( cudaDeviceSynchronize() );

            //  scale_for_FFT<<<scalar_blocks, scalar_threads>>>(
            //     device->d_w_grid_stack, 
            //     num_w_grids_subset, 
            //     config->grid_size, 
            //     1.0/(config->grid_size*config->grid_size)
            // );


            /**************************************************************************************
             * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
             *************************************************************************************/
            // // Copy back and save each w grid to file for review of accuracy
            // std::cout << "Copying back grid stack from device to host..." << std::endl; 
            // proof_of_concept_copy_w_grid_stack_to_host(host, device, config);
            // // Save each w grid into single file of complex type, use python to render via matplotlib
            // std::cout << "Saving grid stack to file..." << std::endl;
            // proof_of_concept_save_w_grids_to_file(host, config, batch);
            /**************************************************************************************
             * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
             *************************************************************************************/

            // if(grid_start_w == 3)
            // {
                // 4. Degrid visibilities from each w-grid stack
                nifty_gridding<VIS_PRECISION, VIS_PRECISION2, PRECISION, PRECISION2, PRECISION3><<<cuda_grid_size, cuda_block_size>>>(
					config->num_visibilities,
					1, // hard-coded number of channels for now!!!
                    device->d_visibilities,
                    device->d_vis_weights,
                    device->d_uvw_coords,
                    device->d_w_grid_stack,
                    config->grid_size,
                    grid_start_w,
                    num_w_grids_subset,
                    config->support,
                    config->beta,
                    //config->upsampling,
                    config->uv_scale,
                    config->w_scale, 
                    config->min_plane_w,
					config->freq / PRECISION(C),
                    config->generating_psf,
                    config->perform_shift_fft,
                    false // degridding
                );
                CUDA_CHECK_RETURN( cudaGetLastError() );
                CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
            // }

            
            // Reset intermediate w grid buffer for next batch
            if(batch < total_w_grid_batches - 1)
            {
                LogF(config, 3, "Resetting device w grid stack memory for next batch...\n");
                CUDA_CHECK_RETURN(cudaMemset(device->d_w_grid_stack, 0, num_w_grid_stack_cells * sizeof(PRECISION2)));
                CUDA_CHECK_RETURN( cudaGetLastError() );
                CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
            }
        }
        CUDA_CHECK_RETURN(cudaMemcpy(host->visibilities, device->d_visibilities, sizeof(VIS_PRECISION2) * config->num_visibilities, cudaMemcpyDeviceToHost));
        save_predicted_visibilities(host, config);
    }

    // Copy back predicted vis from device, send to file...
    //CUDA_CHECK_RETURN(cudaMemcpy(host->visibilities, device->d_visibilities, sizeof(VIS_PRECISION2) * config->num_visibilities, cudaMemcpyDeviceToHost));
    //save_predicted_visibilities(host, config);

    // Copy back final image, clean up device bound buffers
	LogF(config, 3, "Cleaning up allocated buffers.\n");
    nifty_cleanup(device);

    // Finish
}

void nifty_init(host_memory_handles *host, device_memory_handles *device, config *config)
{
    if(device->d_dirty_image == NULL)
    {
        uint32_t num_dirty_image_pixels = config->image_size * config->image_size;
        CUDA_CHECK_RETURN(cudaMalloc(&(device->d_dirty_image), sizeof(PRECISION) * num_dirty_image_pixels));
        CUDA_CHECK_RETURN(cudaMemset(device->d_dirty_image, 0, num_dirty_image_pixels * sizeof(PRECISION)));
        CUDA_CHECK_RETURN( cudaGetLastError() );
        CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
    }

    if(device->d_w_grid_stack == NULL)
    {
        uint32_t num_w_grid_stack_cells = config->grid_size * config->grid_size * config->num_w_grids_batched;
        CUDA_CHECK_RETURN(cudaMalloc(&(device->d_w_grid_stack), sizeof(PRECISION2) * num_w_grid_stack_cells));
        CUDA_CHECK_RETURN(cudaMemset(device->d_w_grid_stack, 0, num_w_grid_stack_cells * sizeof(PRECISION2)));
        CUDA_CHECK_RETURN( cudaGetLastError() );
        CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
    }

    uint32_t num_vis = config->num_visibilities;

    if(device->d_visibilities == NULL)
    {
        CUDA_CHECK_RETURN(cudaMalloc(&(device->d_visibilities), sizeof(VIS_PRECISION2) * num_vis));
        CUDA_CHECK_RETURN(cudaMemcpy(device->d_visibilities, host->visibilities, sizeof(VIS_PRECISION2) * num_vis, cudaMemcpyHostToDevice));
    }

    if(device->d_vis_weights == NULL)
    {
        CUDA_CHECK_RETURN(cudaMalloc(&(device->d_vis_weights), sizeof(VIS_PRECISION) * num_vis));
        CUDA_CHECK_RETURN(cudaMemcpy(device->d_vis_weights, host->vis_weights, sizeof(VIS_PRECISION) * num_vis, cudaMemcpyHostToDevice));
    }

    if(device->d_uvw_coords == NULL)
    {
        CUDA_CHECK_RETURN(cudaMalloc(&(device->d_uvw_coords), sizeof(PRECISION3) * num_vis));
	    CUDA_CHECK_RETURN(cudaMemcpy(device->d_uvw_coords, host->uvw_coords, sizeof(PRECISION3) * num_vis, cudaMemcpyHostToDevice));
    }

    if(device->d_conv_corr_kernel == NULL)
    {
        CUDA_CHECK_RETURN(cudaMalloc(&(device->d_conv_corr_kernel), sizeof(PRECISION) * (config->image_size/2 + 1)));
	    CUDA_CHECK_RETURN(cudaMemcpy(device->d_conv_corr_kernel, host->conv_corr_kernel,
            sizeof(PRECISION) * (config->image_size/2 + 1), cudaMemcpyHostToDevice));
    }

    if(device->fft_plan == NULL)
    {   
        CUDA_CHECK_RETURN(cudaHostAlloc(&(device->fft_plan), sizeof(cufftHandle), cudaHostAllocDefault));

        // Init the plan based on pre-allocated cufft handle (from host mem handles)
        int n[2] = {(int32_t)config->grid_size, (int32_t)config->grid_size};
        CUFFT_SAFE_CALL(cufftPlanMany(
            device->fft_plan,
            2, // number of dimensions per FFT
            n, // number of elements per FFT dimension
            NULL,
            1,
            (int32_t) (config->grid_size * config->grid_size), // num elements per 2d w grid
            NULL,
            1,
            (int32_t) (config->grid_size * config->grid_size), // num elements per 2d w grid
            CUFFT_C2C_PLAN, // Complex to complex
            (int32_t)config->num_w_grids_batched
        ));
    }

    // Note: these three buffers are statically allocated constant device bound arrays, should be located at top of this file
    CUDA_CHECK_RETURN(cudaMemcpyToSymbol(quadrature_nodes,   host->quadrature_nodes,   QUADRATURE_SUPPORT_BOUND*sizeof(PRECISION), 0));
    CUDA_CHECK_RETURN(cudaMemcpyToSymbol(quadrature_weights, host->quadrature_weights, QUADRATURE_SUPPORT_BOUND*sizeof(PRECISION), 0));
    CUDA_CHECK_RETURN(cudaMemcpyToSymbol(quadrature_kernel,  host->quadrature_kernel,  QUADRATURE_SUPPORT_BOUND*sizeof(PRECISION), 0));
}

void nifty_cleanup(device_memory_handles *device)
{
    if(device->d_dirty_image) 
        CUDA_CHECK_RETURN(cudaFree(device->d_dirty_image));
    device->d_dirty_image = NULL;

    if(device->d_w_grid_stack) 
        CUDA_CHECK_RETURN(cudaFree(device->d_w_grid_stack));
    device->d_w_grid_stack = NULL;

    if(device->d_visibilities) 
        CUDA_CHECK_RETURN(cudaFree(device->d_visibilities));
    device->d_visibilities = NULL;

    if(device->d_vis_weights) 
        CUDA_CHECK_RETURN(cudaFree(device->d_vis_weights));
    device->d_vis_weights = NULL;

    if(device->d_uvw_coords) 
        CUDA_CHECK_RETURN(cudaFree(device->d_uvw_coords));
    device->d_uvw_coords = NULL;

    if(device->d_conv_corr_kernel) 
        CUDA_CHECK_RETURN(cudaFree(device->d_conv_corr_kernel));
    device->d_conv_corr_kernel = NULL;

    if(device->fft_plan)
    {
        CUFFT_SAFE_CALL(cufftDestroy(*(device->fft_plan)));
        device->fft_plan = NULL;
    }
}

/**********************************************************************
 * Calculates guass-legendre quadrature values (nodes, weights)
 * and scaled nodes (the conv corr kernel), ie: exp_semicircle() of nodes.
 * Also precalculates fixed half image_size conv kernel (l, m),
 * conv correction for n coord will be done on GPU
 * Note: only need to keep positive n samples (p), due to signed symmetry
 * Note: quadrature_nodes decrease from ~1.0. approaching ~0.0
 * Note: quadrature_kernel increases from 0.0, approaching ~1.0
 **********************************************************************/
void generate_gauss_legendre_conv_kernel(host_memory_handles *host, config *config)
{
    // p based on formula in first paragraph under equation 3.10, page 8 of paper:
    // A parallel non-uniform fast Fourier transform library based on an "exponential of semicircle" kernel
    //uint32_t p = (uint32_t)CEIL(1.5 * config->support + 2.0);
    uint32_t p = (uint32_t)int(1.5 * config->support + 2.0);
    uint32_t n = (uint32_t)(p * 2);

	LogF(config, 9, "legendre p = %i\n", p);

    for (uint32_t i=1; i<=n/2; i++)
    {
        double w_i = 0.0;
        double x_i = calculate_legendre_root((int32_t)i, (int32_t)n, 1e-16, &w_i);
        double k_i = exp(config->beta*(sqrt(1.0 - x_i*x_i) - 1.0)); 
        host->quadrature_nodes[i-1] = (PRECISION)x_i;
        host->quadrature_weights[i-1] = (PRECISION)w_i;
        host->quadrature_kernel[i-1] = (PRECISION)k_i;

		//printf("wgt[%2i] = %.16e, psi[%2i] = %.16e, x[%2i] = %.16e\n", 
		//	i-1, host->quadrature_weights[i-1],
		//	i-1, host->quadrature_kernel[ i-1], 
		//	i-1, host->quadrature_nodes[  i-1]);
    }

    double conv_corr_norm_factor = 0.0;
    for(uint32_t i = 0; i < p; i++)
        conv_corr_norm_factor += host->quadrature_kernel[i]*host->quadrature_weights[i];
    conv_corr_norm_factor *= (double)config->support;

    // Precalculate one side of convolutional correction kernel out to one more than half the image size
    for(uint32_t l_m = 0; l_m <= config->image_size/2; l_m++)
    {
        double l_m_norm =  ((double)l_m)*(1.0/config->grid_size); // between 0.0 .. 0.5
        double correction = 0.0;

        for(uint32_t i = 0; i < p; i++)
            correction += host->quadrature_kernel[i]*cos(PI*l_m_norm*((double)config->support)*host->quadrature_nodes[i])*host->quadrature_weights[i];
		host->conv_corr_kernel[l_m] = (PRECISION)(correction*((double)config->support)) / conv_corr_norm_factor;
		
		if (0) // AG
		{
			printf("cfu[%3i] = (%.16f)\n", l_m, 1.0/((PRECISION)(correction*((double)config->support))));
		}
    } 
	printf("conv_corr_norm_factor = %.16f\n", conv_corr_norm_factor);
}

void copy_dirty_image_to_host(host_memory_handles *host, device_memory_handles *device, config *config)
{
    uint32_t num_dirty_image_pixels = config->image_size * config->image_size;
    CUDA_CHECK_RETURN(cudaMemcpy(host->dirty_image, device->d_dirty_image, num_dirty_image_pixels * sizeof(PRECISION), cudaMemcpyDeviceToHost));
    CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
}

void copy_dirty_image_to_device(host_memory_handles *host, device_memory_handles *device, config *config)
{
    uint32_t num_dirty_image_pixels = config->image_size * config->image_size;
    CUDA_CHECK_RETURN(cudaMemcpy(device->d_dirty_image, host->dirty_image, num_dirty_image_pixels * sizeof(PRECISION), cudaMemcpyHostToDevice));
    CUDA_CHECK_RETURN( cudaDeviceSynchronize() );
}

void save_image_to_file(host_memory_handles *host, config *config, int cycle_number)
{
    char file_name_buffer[257];
    // build file name, ie: my/folder/path/w_grid_123.bin
#if NIFTY_PRECISION == NIFTY_SINGLE
    //snprintf(file_name_buffer, 257, "%s/cng_dirty_image_%i_%i_S.bin", config->data_output_folder, cycle_number, config->image_size);
    snprintf(file_name_buffer, 257, "%s/cng_dirty_image_%i_S.bin", config->data_output_folder, config->image_size);
#else
    //snprintf(file_name_buffer, 257, "%s/cng_dirty_image_%i_%i_D.bin", config->data_output_folder, cycle_number, config->image_size);
    snprintf(file_name_buffer, 257, "%s/cng_dirty_image_%i_D.bin", config->data_output_folder, config->image_size);
#endif
	LogF(config, 3, "Writing image to file: %s ...\n", file_name_buffer);

    //snprintf(file_name_buffer, 257, "%s/dirty_image_%d.bin", config->data_output_folder, cycle_number);
    FILE *f = fopen(file_name_buffer, "wb");
    fwrite(host->dirty_image, sizeof(PRECISION), config->image_size*config->image_size, f);
    fclose(f);
}

void save_image2_to_file(host_memory_handles *host, config *config, int cycle_number)
{
    char file_name_buffer[257];
    // build file name, ie: my/folder/path/w_grid_123.bin
#if NIFTY_PRECISION == NIFTY_SINGLE
    snprintf(file_name_buffer, 257, "%s/CUDA_dirty_image_pws_%i_S.bin", config->data_output_folder, config->image_size);
    snprintf(file_name_buffer, 257, "%s/CUDA_dirty_image_preCC_%i_S.bin", config->data_output_folder, config->image_size);
#else
    snprintf(file_name_buffer, 257, "%s/CUDA_dirty_image_pws_%i_D.bin", config->data_output_folder, config->image_size);
    snprintf(file_name_buffer, 257, "%s/CUDA_dirty_image_preCC_%i_D.bin", config->data_output_folder, config->image_size);
#endif
	LogF(config, 3, "Writing image to file: %s ...\n", file_name_buffer);

    FILE *f = fopen(file_name_buffer, "wb");
    fwrite(host->dirty_image, sizeof(PRECISION), config->image_size*config->image_size, f);
    fclose(f);
}

/**********************************************************************
 * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
 * Copies current batch of computed w grids from device to host 
 * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
 **********************************************************************/
void proof_of_concept_copy_w_grid_stack_to_host(host_memory_handles *host, device_memory_handles *device, config *config)
{
    uint32_t num_w_grid_stack_cells = config->grid_size * config->grid_size * config->num_w_grids_batched;
    CUDA_CHECK_RETURN(cudaMemcpy(host->w_grid_stack, device->d_w_grid_stack, num_w_grid_stack_cells * sizeof(PRECISION2), cudaMemcpyDeviceToHost));
}
 
/**********************************************************************
 * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
 * Saves each computed w grid to simple binary flat file of complex values,
 * only used to inspect if w grid CUDA kernel is behaving correctly per w grid
  * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
 **********************************************************************/
void proof_of_concept_save_w_grids_to_file(host_memory_handles *host, config *config, uint32_t start_w_grid)
{
#if NIFTY_PRECISION == NIFTY_SINGLE
	char precision_char = 'S';
#else
	char precision_char = 'D';
#endif
    char file_name_buffer[257];
    uint32_t num_w_grid_cells = config->grid_size * config->grid_size;
    for(int i = 0; i < config->num_w_grids_batched; i++)
    {
        // build file name, ie: my/folder/path/w_grid_123.bin
        //snprintf(file_name_buffer, 257, "%s/test_w_grid_%d.bin", config->data_output_folder, start_w_grid++);
        snprintf(file_name_buffer, 257, "%s/cng_w_grid_2vis_%d_%d_%c.bin", config->data_output_folder, config->grid_size, start_w_grid++, precision_char);
        //snprintf(file_name_buffer, 257, "%s/cng_w_grid_%d_%d_%c.bin", config->data_output_folder, config->grid_size, start_w_grid++, precision_char);
	    LogF(config, 1, "Writing %s ... ", file_name_buffer);
        FILE *f = fopen(file_name_buffer, "wb");

        // memory offset for "splitting" the binary write process
        uint32_t w_grid_index_offset = i * num_w_grid_cells; 
        fwrite(host->w_grid_stack + w_grid_index_offset, sizeof(PRECISION2), num_w_grid_cells, f);
        
        fclose(f);
	    LogF(config, 1, "done.\n");
    }
}

/**********************************************************************
 * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
 * Saves each computed w grid to simple binary flat file of complex values,
 * only used to inspect if w grid CUDA kernel is behaving correctly per w grid
  * TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY - TESTING FUNCTIONALITY ONLY
 **********************************************************************/
void proof_of_concept_save_w_images_to_file(host_memory_handles *host, config *config, uint32_t start_w_grid)
{
    char file_name_buffer[257];
    uint32_t num_w_grid_cells = config->grid_size * config->grid_size;
    for(int i = 0; i < config->num_w_grids_batched; i++)
    {
        // build file name, ie: my/folder/path/w_grid_123.bin
        snprintf(file_name_buffer, 257, "%s/cw_image_%d.bin", config->data_output_folder, start_w_grid++);
        FILE *f = fopen(file_name_buffer, "wb");

        // memory offset for "splitting" the binary write process
        uint32_t w_grid_index_offset = i * num_w_grid_cells; 
        fwrite(host->w_grid_stack + w_grid_index_offset, sizeof(PRECISION2), num_w_grid_cells, f);
        
        fclose(f);
    }
}

/**********************************************************************
* Calculate the n-th Legendre Polynomial P_n at x
* optionally also calculates the (first) derivate P_n' at x when -1<x<1
**********************************************************************/
double get_legendre(double x, int n, double *derivative)
{
    if (n==0)
    {
        if (derivative != NULL)
             *derivative = 0.0;
        return 1.0;
    }
    else if (n==1)
    {
        if (derivative != NULL)
             *derivative = 1.0;
        return x;
    }
    // else n>=2

    // recursively calculate P_n(x) = ((2n-1)/n)xP_{n-1}(x) - ((n-1)/n)P_{n-2}(x)
    // note this is same as P_n(x) = xP_{n-1}(x) + ((n-1)/n)(xP_{n-1}(x)-P_{n-2}(x))
    double p_im2 = 1.0; // P_{i-2}(x)
    double p_im1 = x; // P_{i-1}(x)
    double p_i;

    for (int32_t i=2; i<=n; i++)
    {
        p_i = x*p_im1 + (((double)i-1.0)/(double)i)*(x*p_im1-p_im2);
        // prepare for next iteration if required
        p_im2 = p_im1;
        p_im1 = p_i;
    }

    // optionally calculate the derivate of P_n at x
    // note this is given by P_n'(x) = n/(1-x*x)(P_{n-1}(x)-x*P_n(x))
    if (derivative != NULL && x>-1.0 && x<1.0)
    {
        *derivative = (double)n/(1.0-(x*x))*(p_im2-x*p_i); // note p_im2 currently holds P_{n-1}(x)
    }
    return p_i;
}

/**********************************************************************
* Calculate an initial estimate of i-th root of n-th Legendre Polynomial
* where i=1,2,...,n ordered from largest root to smallest
* Note this follows the approach using in equation 13 of the paper
* "Sugli zeri dei polinomi sferici ed ultrasferici" by Francesco G. Tricomi
* Annali di Matematica Pura ed Applicata volume 31, pages93–97 (1950)
**********************************************************************/
double get_approx_legendre_root(int32_t i, int32_t n)
{
    double improvement = 1.0 - (1.0-1.0/(double)n)/(8.0*(double)n*(double)n);
    double estimate = cos(M_PI*(4.0*(double)i-1.0)/(4.0*(double)n+2.0));
    return improvement*estimate;
}

/**********************************************************************
* Calculates an estimate of i-th root of n-th Legendre Polynomial
* where i=1,2,...,n ordered from largest root to smallest
* using Newton-Ralphson iterations until the specified
* accuracy is reached and then also calculate its weight for Gauss-Legendre quadrature
**********************************************************************/
double calculate_legendre_root(int32_t i, int32_t n, double accuracy, double *weight)
{
    double next_estimate = get_approx_legendre_root(i, n);
    double derivative;
    double estimate;
    int32_t iterations = 0;
    do
    {
        estimate = next_estimate;
        double p_n = get_legendre(estimate, n, &derivative);
        next_estimate = estimate - p_n/derivative;
        iterations++;
    }
    while (fdim(next_estimate,estimate)>accuracy && iterations<MAX_NEWTON_RAPHSON_ITERATIONS);

    // Gauss-Legendre quadrature weight for x is given by w = 2/((1-x*x)P_n'(x)*P_n'(x))
    double p_n = get_legendre(next_estimate, n, &derivative);
    *weight = 2.0/((1.0-next_estimate*next_estimate)*derivative*derivative);
    return next_estimate;
}

template<typename FP>
void generate_synthetic_visibilities(host_memory_handles *host, config *config)
{
    host->visibilities[0] = MAKE_VIS_PRECISION2(1.0, 10.0);
    host->vis_weights[0]  = (VIS_PRECISION) 1.0;
    host->uvw_coords[0]   = MAKE_PRECISION3(
        -11270.58405924,  
        992.09566319,   
         0.0  // = 17.0141
    );

    host->visibilities[1] = MAKE_VIS_PRECISION2(1.0, 10);
    host->vis_weights[1]  = (VIS_PRECISION) 1.0;
    host->uvw_coords[1]   = MAKE_PRECISION3(
        12116.9140802,  
        13300.52096583,   
         500.0  // = 8.6750
    );
	
    host->visibilities[2] = MAKE_VIS_PRECISION2(1.0, 10);
    host->vis_weights[2]  = (VIS_PRECISION) 1.0;
    host->uvw_coords[2]   = MAKE_PRECISION3(
        -10000.0,  
        -10000.0,  
         1770.405762// = 10.0069
    );
}

void load_visibilities_from_file(host_memory_handles *host, config *config)
{
    //FILE *vis_intensities = fopen(config->vis_intensity_file, "rb");
    //FILE *vis_uvw = fopen(config->vis_uvw_file, "rb");

	// NB: these names are hard-coded!!
#if VISIBILITY_PRECISION == NIFTY_SINGLE
    FILE *vis_intensities = fopen("/home/seth/Desktop/Anthony/CUDA_Nifty/data/HIPPO_vis_S.bin", "rb");
    FILE *vis_uvw         = fopen("/home/seth/Desktop/Anthony/CUDA_Nifty/data/HIPPO_uvw_S.bin", "rb");
#else
    FILE *vis_intensities = fopen("/home/seth/Desktop/Anthony/CUDA_Nifty/data/HIPPO_vis_D.bin", "rb");
    FILE *vis_uvw         = fopen("/home/seth/Desktop/Anthony/CUDA_Nifty/data/HIPPO_uvw_D.bin", "rb");
#endif

    if(vis_intensities == NULL || vis_uvw == NULL)
    {
        LogF(config, 0, "ERR: Unable to locate specified visibility files\n");
        exit(EXIT_FAILURE);
    }

    uint32_t num_vis = 0;
    fread(&num_vis, sizeof(uint32_t), 1, vis_intensities); // basically ignore this
    LogF(config, 3, "Intensity vis num: %d\n", num_vis);
    fread(&num_vis, sizeof(uint32_t), 1, vis_uvw);         // basically ignore this
    LogF(config, 3, "Uvw vis num: %d\n", num_vis);

    PRECISION2* temp_intensities = (PRECISION2*) calloc(config->num_visibilities, sizeof(PRECISION2));
    PRECISION3* temp_uvw = (PRECISION3*) calloc(config->num_visibilities, sizeof(PRECISION3));
    uint32_t num_intensities_found = fread(temp_intensities, sizeof(PRECISION2), config->num_visibilities, vis_intensities);
    uint32_t num_uvw_found = fread(temp_uvw, sizeof(PRECISION3), config->num_visibilities, vis_uvw);

    fclose(vis_intensities);
    fclose(vis_uvw);

    if(num_intensities_found != config->num_visibilities || num_uvw_found != config->num_visibilities)
    {
        LogF(config, 3, "ERR: Number of visibilities read from file does not match NIFTy config\n");
        exit(EXIT_FAILURE);
    }

    PRECISION min_abs_w_meters = FLT_MAX;
    PRECISION max_abs_w_meters = FLT_MIN;

    for(int v = 0; v < config->num_visibilities; v++)
    {
        host->visibilities[v] = MAKE_VIS_PRECISION2(temp_intensities[v].x, temp_intensities[v].y);
        host->vis_weights[v]  = (VIS_PRECISION) 1.0;
        host->uvw_coords[v]   = temp_uvw[v];

        LogF(config, 10, "Vis %3i, is at (%8.3f, %8.3f, %8.3f) with value [%8.3f, %8.3fj]\n",
			v, host->uvw_coords[v].x, host->uvw_coords[v].y, host->uvw_coords[v].z, host->visibilities[v].x, host->visibilities[v].y);
		
        // if(host->uvw_coords[v].z < 0.0)
        // {
        //     host->uvw_coords[v].x *= -1.0; // flip w to positive
        //     host->uvw_coords[v].y *= -1.0; // flip w to positive
        //     host->uvw_coords[v].z *= -1.0; // flip w to positive

        //     host->visibilities[v].y *= -1.0; // conjugate
        // }

        //PRECISION w_to_meters = ABS(temp_uvw[v].z) / (config->freq / C);
        PRECISION w_to_meters = ABS(temp_uvw[v].z);
        if(w_to_meters < min_abs_w_meters)
            min_abs_w_meters = w_to_meters;
        if(w_to_meters > max_abs_w_meters)
            max_abs_w_meters = w_to_meters;
    }

    LogF(config, 3, "Measured min abs w meters: %.16e\n", min_abs_w_meters);
    LogF(config, 3, "Measured max abs w meters: %.16e\n", max_abs_w_meters);

    free(temp_intensities);
    free(temp_uvw);
}

void save_predicted_visibilities(host_memory_handles *host, config *config)
{
    char intensity_file_name_buffer[257];
    //snprintf(intensity_file_name_buffer, 257, "%s/predicted_vis.bin", config->data_output_folder);
#if VISIBILITY_PRECISION == NIFTY_SINGLE
    snprintf(intensity_file_name_buffer, 257, "%s/cu_predicted_vis_%i_S.bin", config->data_output_folder, config->image_size);
#else
    snprintf(intensity_file_name_buffer, 257, "%s/cu_predicted_vis_%i_D.bin", config->data_output_folder, config->image_size);
#endif
    FILE *vis_intensities = fopen(intensity_file_name_buffer, "wb");

    if(vis_intensities == NULL)
    {
        LogF(config, 3, "ERR: Unable to save visibilities to file\n");
        exit(EXIT_FAILURE);
    }

    //fwrite(&(config->num_visibilities), sizeof(int), 1, vis_intensities);

    // Record individual visibilities
    for(int v = 0; v < config->num_visibilities; ++v)
    {
        VIS_PRECISION2 current_vis = host->visibilities[v];

        //if(abs(current_vis.x) > 0.00001)
        //    printf("PREDICTED VIS %d is %f, %f\n",v, current_vis.x, current_vis.y);


        fwrite(&current_vis, sizeof(VIS_PRECISION2), 1, vis_intensities);
    }

    fclose(vis_intensities);
}

void check_cuda_error_aux(const char *file, uint32_t line, const char *statement, cudaError_t err)
{
	if (err == cudaSuccess)
    {
        // printf("CHECK_CUDA SUCCESS: %s...\n", cudaGetErrorString(err));
		return;
    }

	printf(">>> CUDA ERROR: %s returned %s in %s : line %u ", statement, cudaGetErrorString(err), file, line);
	//printf(">>> CUDA ERROR: %s returned %i in %s : line %u ", statement, err, file, line);

	exit(EXIT_FAILURE);
}

void cufft_safe_call(cufftResult err, const char *file, const int line)
{
    if( CUFFT_SUCCESS != err) {
		printf("CUFFT error in file '%s', line %d\nerror %d: %s\nterminating!\n",
			file, line, err, cuda_get_error_enum(err));
		cudaDeviceReset();
    }
}

const char* cuda_get_error_enum(cufftResult error)
{
    switch (error)
    {
        case CUFFT_SUCCESS:
            return "CUFFT_SUCCESS";

        case CUFFT_INVALID_PLAN:
            return "CUFFT_INVALID_PLAN";

        case CUFFT_ALLOC_FAILED:
            return "CUFFT_ALLOC_FAILED";

        case CUFFT_INVALID_TYPE:
            return "CUFFT_INVALID_TYPE";

        case CUFFT_INVALID_VALUE:
            return "CUFFT_INVALID_VALUE";

        case CUFFT_INTERNAL_ERROR:
            return "CUFFT_INTERNAL_ERROR";

        case CUFFT_EXEC_FAILED:
            return "CUFFT_EXEC_FAILED";

        case CUFFT_SETUP_FAILED:
            return "CUFFT_SETUP_FAILED";

        case CUFFT_INVALID_SIZE:
            return "CUFFT_INVALID_SIZE";

        case CUFFT_UNALIGNED_DATA:
            return "CUFFT_UNALIGNED_DATA";

        case CUFFT_INCOMPLETE_PARAMETER_LIST:
        	return "CUFFT_INCOMPLETE_PARAMETER_LIST";

    	case CUFFT_INVALID_DEVICE:
    		return "CUFFT_INVALID_DEVICE";

        case CUFFT_PARSE_ERROR:
            return "CUFFT_PARSE_ERROR";

        case CUFFT_NO_WORKSPACE:
            return "CUFFT_NO_WORKSPACE";

        case CUFFT_NOT_IMPLEMENTED:
            return "CUFFT_NOT_IMPLEMENTED";

        case CUFFT_LICENSE_ERROR:
            return "CUFFT_LICENSE_ERROR";

        case CUFFT_NOT_SUPPORTED:
            return "CUFFT_NOT_SUPPORTED";
        }

        return "<unknown>";
}

void LogF(config *config, int thisLevel, const char * format, ... )
{
	if (thisLevel <= config->log_level)
	{
		va_list args;
		va_start (args, format);
		vprintf (format, args);
		va_end (args);
	}
}
